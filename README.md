#** README - UTKAST **

##** SEBRA - applikation för biverkningsrapportering **

STATUS - både applikationen och dokumentationen är under utveckling

##** Sätta upp utvecklingsmiljön **

### Detta behöver du:   
- Git
- NodeJS och npm; https://nodejs.org/en/download/   
angular-cli; npm install -g @angular/cli   
- MongoDB; https://docs.mongodb.com/manual/administration/install-community/    

* Klona repot

##** Bygga applikationen **
- cd \<project root>
- gradlew clean build

##** Köra applikationen lokalt **
* starta lokal mongo-server   
(på windows:   
mongod.exe --dbpath <data dir, e.g. E:\dev\mongo\data\db> )   
* starta applikationen, jar-en är körbar:   
<project root>/build/libs/sebra-<version>.jar   
eller   
cd <project root>   
gradle bootRun  
* GUI-t ska nu vara tillgängligt på http://localhost:8080/index.html   
* Under utveckling av GUI-delen (angular-applikationen) kan denna även köras separat, i "hot swap"-läge (dvs ändringar i koden slår igeom direkt i körande applikation) med:    
cd <project root>/gui   
npm start   
(vilket är konfigurerat att köra   
ng serve --proxy-config proxy.conf.json --open),   
Aktuell version av GUI-t visas då på http://localhost:4200

##** Köra applikationen på testservern sebra.inera.nordicmedtest.se **
Applikationen är uppsatt som en tjänst (init.d) på denna maskin, och kan startas och stoppas samt ge status med:   
service sebra [start|stop|status]
 
##** Byggserver - Ineras Jenkins-instans **
På Ineras Jenkinsinstans, https://ci.inera.se/, finns en pipeline för projektet; https://ci.inera.se/job/SEBRA/job/sebra-build/   

Denna pipeline är specificerad i den Jenkinsfile som finns i detta repo (direkt i roten, dvs direkt under Source i menyn till vänster)   

Just nu är den konfigurerad att trigga på incheckning och då bygga projektet, inkl köra de automatiska tester som finns i projektet och sedan deploya till testservern sebra.inera.nordicmedtest.se. För detaljer, se Jenkinsfilen.

##** SOAP-UI-testsvit **
I domän-repot finns en soapui-testsvit:   
https://bitbucket.org/rivta-domains/riv.lv.reporting.pharmacovigilance/src/fce355b165c2ecad984dff2e900caafeb72fc295/test-suite/?at=master   
<TODO: uppdatera länk till nya domän-repot efter flytt>

Denna innehåller även en soap-ui-mock-tjänst, som man kan anropa från Sebra. SOAP-UI kan konfigureras att kräva SSL med klient-autentisering, under File -> Preferences -> SSL

##** URL-er och logiska adresser till tjänsterna via tjänsteplattformen **

** i SIT: **

-  url: https://test.esb.ntjp.se:443/vp/lv/reporting/pharmacovigilance/ProcessIndividualCaseSafetyReport/1/rivtabp21   
bör vara åtkomlig från Ineras nät  
- (förväntat server-) certifikat: sebra.inera.nordicmedtest.se.p12, som ligger på sebra.inera.nordicmedtest.se under /home/inera/sebra.inera.nordicmedtest.se.p12      
(pw hos Niclas / Ann-Sofie)   
- logisk adress: 2021004078   

** i QA: **

- url: TODO
- certifikat: TODO
- logisk adress: 

** i PROD: **

- url: TODO
- certifikat: TODO
- logisk adress: TODO

Loggar för TP SIT kan man se här (uppkopplad mot basefarm, tex via vpn):   
http://ine-tib-kibana1.in1.sth.basefarm.net:5601/app/kibana   
(motsvarande för QA:   
http://ine-sib-kibana1.in1.sth.basefarm.net:5601/app/kibana) 

Generell info om tjänsteplattformen finns här:   
https://skl-tp.atlassian.net/wiki/spaces/NTJP/pages/6815746/Support+NTjP   
och info om vilka tjänster (konsumenter, producenter, logiska adresser) som är upplagda i respektive tjänsteplattform:   
https://integrationer.tjansteplattform.se/views/multiview/multiviewv4.html   


##** Gränssnitt till "Axel" **
För att sedan se om meddelandet nått "Axel" kan man använda följande gränssnitt (uppkopplad mot basefarm, tex via vpn):   
http://ine-tit-app01.sth.basefarm.net:8080/shs-broker/   
  
##** Generera om domän-klasser vid schema-uppdateringar **

Schema-filer för domänen hämtas från respektive domän-repo, justeras för smarttags, och används för att generera java-klasser som används av sebra-applikationen. 
Detta görs med hjälp av en maven-pom som även den utgår från motsvarande domän-repo men har utökats för att mappa paketnamn.

När scheman i domänen uppdateras genereras koden för sebra om enligt följande:   
1. kopiera in uppdaterade scheman, just nu från (kommer att delas upp i flera repon)   
https://bitbucket.org/rivta-domains/riv.lv.reporting.pharmacovigilance/src/64ff294a8635f06126fd095ee482a10686736dbb/schemas/?at=master   
till   
https://bitbucket.org/ineraservices/sebra/src/73b4847d78f633f6b5a0fa238f960cb1928391d4/src/main/resources/schemas/domain/?at=master   
och   
https://bitbucket.org/ineraservices/sebra/src/73b4847d78f633f6b5a0fa238f960cb1928391d4/src/main/resources/schemas/sebra_mod/?at=master   
2. i schemas/sebra_mod, gör följande modifieringar: 
 - kommentera ut elementet smarttag ur smarttags.xsd (se tidigare versioner av samma fil i sebra-repot)   
 - uppdatera lv_reporting_pharmacovigilance_1.0.xsd så att den importerar smarttags.xsd och använder typen SmartTagType för elementet parentmedicalrelevanttext (se tidigare versioner av samma fil i sebra-repot)   
3. generera ny java-kod från dessa modifierade scheman med:   
cd <local project path>/src/sup   
mvn -f ./custom_biv_jaxb_pom.xml clean generate-sources   
4. kopiera in den genererade koden under src/main/java, dvs från:   
src/sup/target/generated/src/main/java/    
till   
src/main/java/   








#######################################################################################################
###** DEFAULT TEXT NEDAN - VI BEHÅLLER DEN FÖR REFERENS TILLS VIDARE **
########################################################################################################

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact