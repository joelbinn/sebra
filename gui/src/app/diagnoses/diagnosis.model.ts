/**
 * For mapping an object of type report.individualCaseSafetyReport.ichicsr.safetyreport.patient.medicalhistoryepisode
 */

export class Diagnosis {
  private _patientepisodename: string;
  private _patientmedicalstartdateformat: number;
  private _patientmedicalstartdate: number; // date

  get name(): string {
    return this._patientepisodename;
  }

  get dateFormat(): number {
    return this._patientmedicalstartdateformat;
  }

  get startDate(): number {
    return this._patientmedicalstartdate;
  }

  withName(value: string) {
    this._patientepisodename = value;
    return this;
  }

  withDateformat(value: number) {
    this._patientmedicalstartdateformat = value;
    return this;
  }

  withStartDate(value: number) {
    this._patientmedicalstartdate = value;
    return this;
  }

}
