import {Component, Input, OnInit} from '@angular/core';
import {SelectItem} from "primeng/primeng";
import {Diagnosis} from "./diagnosis.model";
import * as utils from "lodash";

@Component({
  selector: 'sebra-diagnoses',
  templateUrl: './diagnoses.component.html',
  styleUrls: ['./diagnoses.component.css']
})

export class DiagnosesComponent implements OnInit {

  @Input()
  patientDiagnoses: Diagnosis[]; // Belong to loaded report
  diagnosisCatalog: Diagnosis[] = ICD_10_DIAGNOSES; // Should be a service
  filteredDiagnoses: SelectItem[];
  selectedDiagnosis: SelectItem;

  constructor() {
  }

  ngOnInit() {
  }

  showInfo() {
    console.debug('Show info on component: ', this.constructor.name);
  }

  doFilterDiagnoses(selectedDiagnos: any) {
    this.filteredDiagnoses = this.diagnosisCatalog
      .filter(diagnos => diagnos.name.toLowerCase().indexOf(selectedDiagnos.query.toLowerCase()) >= 0)
      .map(diagnos => {
        return <SelectItem> {label: diagnos.name, value: diagnos.name}
      });
  }

  newDiagnosis() {
    const newDaignos = new Diagnosis().withName(this.selectedDiagnosis.label) ;
    this.patientDiagnoses.push(newDaignos);
    this.selectedDiagnosis = null;
  }

  setSelectedDiagnosis(event: any) {
    if (typeof event != "string") {
      this.selectedDiagnosis = event;
    }
  }

  removeDiagnosis(diagnosis: Diagnosis) {
    utils.remove(this.patientDiagnoses, (diag => diag.name === diagnosis.name))
  }
} // CLASS

const ICD_10_DIAGNOSES: Diagnosis[] = [
  new Diagnosis().withName("R10.4X: Buksmärtor UNS"),
  new Diagnosis().withName("H10-H13: Sjukdomar i bindehinnan"),
  new Diagnosis().withName("I00-I02: Akut reumatisk feber")
];
