import {Diagnosis} from "./diagnosis.model";

export class Diagnoses {

  list: Diagnosis[];

  constructor(mheList: Array<any>) {
    this.list = mheList.map(medicalhistoryepisode =>  new Diagnosis()
      .withName(medicalhistoryepisode.patientepisodename)
      .withDateformat(medicalhistoryepisode.patientmedicalstartdateformat)
      .withStartDate(medicalhistoryepisode.patientmedicalstartdate)
    );
  }


}
