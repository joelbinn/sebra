import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DiagnosesComponent} from "./diagnoses.component";
import {SebraCommonModule} from "../sebra-common/sebra-common.module";
import {AutoCompleteModule} from "primeng/primeng";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    SebraCommonModule,
    AutoCompleteModule,
    FormsModule // needed for the AutoCompleteModule
  ],
  declarations: [DiagnosesComponent],
  exports: [DiagnosesComponent]
})

export class DiagnosesModule {
}
