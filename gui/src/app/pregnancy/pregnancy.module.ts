import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {PregnancyComponent} from './pregnancy.component';
import {DropdownModule} from "primeng/primeng";

@NgModule({
  declarations: [
    PregnancyComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    DropdownModule
  ],
  exports: [
    PregnancyComponent
  ],

  providers: [],
})
export class PregnancyModule {
}
