import {Component} from '@angular/core';
import {SelectItem} from "primeng/primeng";

@Component({
  selector: 'sebra-pregnancy',
  templateUrl: './pregnancy.component.html',
})

export class PregnancyComponent {
  gestationPeriodOptions = GESTATION_OPTIONS;
  gestationPeriod: SelectItem;
}

const GESTATION_OPTIONS: SelectItem[] = [
  {label:"Trimester", value:810},
  {label:"Månad", value:802},
  {label:"Vecka", value:803},
  {label:"Dag", value:804}
];
