import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sebra-reporter-opinion',
  templateUrl: './reporter-opinion.component.html',
  styleUrls: ['./reporter-opinion.component.css']
})
export class ReporterOpinionComponent implements OnInit {

  @Input()
  narrativeIncludeClinical: string;

  constructor() { }

  ngOnInit() {
  }

  showInfo() {
    console.debug('Show info on component: ', this.constructor.name);
  }
}
