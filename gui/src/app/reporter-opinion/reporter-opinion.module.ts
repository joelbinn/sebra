import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {InputTextareaModule} from 'primeng/primeng';
import {SebraCommonModule} from "../sebra-common/sebra-common.module";
import {ReporterOpinionComponent} from "./reporter-opinion.component";

@NgModule({
  imports: [
    CommonModule,
    InputTextareaModule,
    FormsModule, // needed for InputTextareaModule
    SebraCommonModule
  ],
  declarations: [ReporterOpinionComponent],
  exports: [ReporterOpinionComponent]
})

export class ReporterOpinionModule {
}
