import {
  Component,
  EventEmitter,
  Output
} from "@angular/core";

@Component({
  selector: 'sebra-info-button',
  template: `
    <a href="javascript:void(0);" style="text-decoration: none;" (click)="handleClick()">
        <span style="font-size: small;" class="fa-stack">
          <i class="fa fa-circle text-danger fa-stack-2x"></i>
          <i class="fa fa-info fa-inverse fa-stack-1x"></i>
        </span>
    </a>
  `
})
export class InfoButtonComponent {
  @Output()
  onClick: EventEmitter<void> = new EventEmitter<void>();

  handleClick() {
    this.onClick.next();
  }
}
