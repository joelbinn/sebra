import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";

@Component({
  selector: 'sebra-date',
  template: `
    <div style="width: 9em;">
      <p-calendar [inputStyle]="{width:'7em'}" [showIcon]="true" dateFormat="yy-mm-dd" [ngModel]="jsdate" (ngModelChange)="updateDate($event)">
      </p-calendar>
    </div>
  `
})
export class SebraDateComponent implements OnInit {
  @Input()
  date: string;
  @Output()
  dateChange: EventEmitter<string> = new EventEmitter();

  @Input()
  format: string;
  @Output()
  formatChange: EventEmitter<string> = new EventEmitter();

  jsdate: Date;

  ngOnInit() {
    //custom format 102=CCYYMMDD
    //iso-format: YYYY-MM-DD
    //TODO: use lib date-and-time instead (npm install --save date-and-time)
    if (this.date != null) {
      const year = Number(this.date.substr(0, 4));
      const month: number = (Number(this.date.substr(4, 2))) - 1;
      const day = Number(this.date.substr(6, 2));
      this.jsdate = new Date(year, month, day);
    }
  }

  updateDate(dateEvent: Date) {
    if (dateEvent !== null) {
      this.jsdate = dateEvent;
      this.date = this.dateToString(dateEvent, this.format);
    }

    this.dateChange.emit(this.date);
  }

  private dateToString(date: Date, format: string): string {
    //TODO: parse based on format!! if different formats are needed, Malin checks
    return date.toISOString();
  }

}
