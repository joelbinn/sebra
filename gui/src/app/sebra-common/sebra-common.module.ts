import {NgModule} from "@angular/core";
import {InfoButtonComponent} from "./info-button.component";
import {SebraDateComponent} from "./sebra-date.component";
import {CalendarModule} from "primeng/primeng";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    InfoButtonComponent,
    SebraDateComponent
  ],
  imports: [
    CalendarModule,
    FormsModule
  ],
  exports: [
    InfoButtonComponent,
    SebraDateComponent
  ]
})
export class SebraCommonModule {

}
