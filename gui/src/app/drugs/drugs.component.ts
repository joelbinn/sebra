import {Component, Input} from '@angular/core';
import {Patient} from "../model/patient.model";
import {SelectItem} from "primeng/primeng";
import {DrugList} from "../model/druglist.model";
import {Drug} from "../model/drug.model";
import {NgModel} from "@angular/forms";
import * as _ from "lodash";

@Component({
  selector: 'sebra-drugs',
  templateUrl: './drugs.component.html',
})

export class DrugsComponent {

  @Input()
  patient: Patient;

  @Input()
  druglist: DrugList;

  drugcharacterizations: SelectItem[] = [
    {label: "Övrigt", value: "2"},
    {label: "Misstänkt", value: "1"}
  ];

  drugReactDiscontinueOptions: SelectItem[] = [
    {label: "Ja", value: "Ja"},
    {label: "Nej", value: "Nej"},
    {label: "Okänt", value: "Okänt"},
    {label: "Ej utsatt", value: "Ej utsatt"},
  ];

  drugReactReturnedOptions: SelectItem[] = [
    {label: "Ja", value: "Ja"},
    {label: "Nej", value: "Nej"},
    {label: "Okänt", value: "Okänt"},
    {label: "Ej återinsatt", value: "Ej återinsatt"},
  ];

  drugCatalogue: SelectItem[] = [
    {
      label: "Alvedon 500mg",
      value: {medicinalproduct: "Alvedon", drugdosageform: "Filmdragerad tablett", intensity: "500mg"}
    },
    {
      label: "Alvedon Novum",
      value: {medicinalproduct: "Alvedon Novum", drugdosageform: "Filmdragerad tablett", intensity: "500mg"}
    },
  ];

  filteredDrugs: SelectItem[];
  selectedDrug: SelectItem;

  trackByMedicinalProduct(index: number, drug: Drug) {
    return drug.medicinalproduct;
  }

  showInfo() {
    console.debug('Show info on drugs component');
  }

  //TODO: tmp method to trouble-shoot data-binding, remove this (and it's use)
  logDrugCharacterization($event: string) {
    console.log("DrugsComponent.logDrugCharacterization, ngModel changed, event: ", $event);
    console.log("this.druglist.drugs[0].drugcharacterization: ", this.druglist.drugs[0].drugcharacterization);
    //console.log("updating this.druglist.drugs[0].drugcharacterization to ", $event);
    //this.druglist.drugs[0].drugcharacterization = $event;
  }

  patientFirstName(): string {
    return this.patient.firstName;
  }

  getErrorKeys(drugDosageText: NgModel): string[] {
    return _.keys(drugDosageText.errors);
  }

  errorDescription(key: string): string {
    return ERROR_DESCRIPTIONS[key];
  }

  findDrug(drugInput: any) {
    this.filteredDrugs = this.drugCatalogue.filter(selectedDrug => selectedDrug.label.toLowerCase().indexOf(drugInput.query.toLowerCase()) >= 0);
  }

  addDrug() {
    this.druglist.add(this.selectedDrug.value);
    this.selectedDrug = null;
  }

  setSelectedDrug(event: any) {
    if (typeof event != "string") {
      this.selectedDrug = event;
    }
  }

  removeDrug(drug: Drug) {
    this.druglist.remove(drug);
  }
}

const ERROR_DESCRIPTIONS = {
  required: "Värde måste anges",
  batchNumber: "Max 35 tecken"
}
