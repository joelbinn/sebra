import {Component, Input, OnInit} from "@angular/core";
import {Drug} from "../model/drug.model";
import {SelectItem} from "primeng/primeng";
import {NgModel} from "@angular/forms";

@Component({
  selector: 'sebra-admin-route-cell',
  template: `
    <div style="width: 10em;">
      <p-autoComplete appendTo="body" [ngModel]="selectedAdminRoute" (ngModelChange)="updateAdminRoute($event)"
                      [suggestions]="filteredAdminRoutes"
                      (completeMethod)="findAdminRoutes($event)"
                      (onBlur)="clearIfNotSelected(model)"
                      [dropdown]="true"
                      [size]="7"
                      field="label" #model="ngModel">
      </p-autoComplete>
    </div>
  `
})
export class AdminRouteCellComponent implements OnInit {
  @Input()
  drug: Drug;
  selectedAdminRoute: SelectItem;
  filteredAdminRoutes: SelectItem[];

  ngOnInit(): void {
    this.selectedAdminRoute = SELECTABLE_ADMIN_ROUTES.find(item => item.value === this.drug.drugadministrationroute);
    console.debug('selectedAdminRoute:', this.selectedAdminRoute);
  }

  findAdminRoutes(event: any) {
    console.debug('findAdminRoutes event:', event);
    console.debug('selectedAdminRoute:' + this.selectedAdminRoute);
    this.filteredAdminRoutes = SELECTABLE_ADMIN_ROUTES.filter(selectItem => selectItem.label.search(new RegExp(event.query, "i")) >= 0);
    console.debug('filteredAdminRoutes:', this.filteredAdminRoutes);
  }

  clearIfNotSelected(model: NgModel) {
    console.log("on blur - reset model");
    if (this.selectedAdminRoute == undefined) {
      model.reset(undefined);
    }
  }

  updateAdminRoute($event: any) {
    console.log("in updateAdminRoute, event: ", $event);
    if ($event != undefined && typeof $event != 'string') {
      this.selectedAdminRoute = $event;
      this.drug.drugadministrationroute = this.selectedAdminRoute.value;
    }
  }
}

const SELECTABLE_ADMIN_ROUTES: SelectItem[] = [
  {value: "001", label: "Auricular (otic)"},
  {value: "002", label: "Buccal"},
  {value: "003", label: "Cutaneous"},
  {value: "004", label: "Dental"},
  {value: "005", label: "Endocervical"},
  {value: "006", label: "Endosinusial"},
  {value: "007", label: "Endotracheal"},
  {value: "008", label: "Epidural"},
  {value: "009", label: "Extra-amniotic"},
  {value: "010", label: "Hemodialysis"},
  {value: "011", label: "Intra corpus cavernosum"},
  {value: "012", label: "Intra-amniotic"},
  {value: "013", label: "Intra-arterial"},
  {value: "014", label: "Intra-articular"},
  {value: "015", label: "Intra-uterine"},
  {value: "016", label: "Intracardiac"},
  {value: "017", label: "Intracavernous"},
  {value: "018", label: "Intracerebral"},
  {value: "019", label: "Intracervical"},
  {value: "020", label: "Intracisternal"},
  {value: "021", label: "Intracorneal"},
  {value: "022", label: "Intracoronary"},
  {value: "023", label: "Intradermal"},
  {value: "024", label: "Intradiscal (intraspinal)"},
  {value: "025", label: "Intrahepatic"}
];

