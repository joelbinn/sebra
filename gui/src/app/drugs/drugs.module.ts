import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {DrugsComponent} from './drugs.component';
import {AutoCompleteModule, DropdownModule, InputTextModule} from 'primeng/primeng';
import {AdminRouteCellComponent} from "./admin-route-cell.component";
import {SebraCommonModule} from "../sebra-common/sebra-common.module";
import {CustomValidator} from "./custom-validator";

@NgModule({
  declarations: [
    DrugsComponent,
    AdminRouteCellComponent,
    CustomValidator
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    DropdownModule,
    AutoCompleteModule,
    SebraCommonModule,
    InputTextModule
  ],
  exports: [
    DrugsComponent
  ],

  providers: [],
})
export class DrugsModule {
}
