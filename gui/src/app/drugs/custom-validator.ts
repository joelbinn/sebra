import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

@Directive({selector: "[batchNumber]", providers: [{provide: NG_VALIDATORS, useExisting: CustomValidator, multi: true}]})
export class CustomValidator implements Validator {


  validate(input: AbstractControl): ValidationErrors | any {
    return !input.value || input.value.length <= 35 ? null : {"batchNumber": null};
  }

}
