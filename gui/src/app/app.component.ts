import {Component, Inject, OnInit} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {EXAMPLE_REPORT, ReportWrapperModel} from "./model/report-wrapper.model";
import {DOCUMENT} from "@angular/common";
import {BackendService} from "./backend.service";
import {isNull, isNullOrUndefined} from "util";
import {SafetyReport} from "./model/safety-report.model";
import {Patient} from "./model/patient.model";
import {PatientImpl} from "./model/patient-impl.model";

//TODO: handle async backend calls so that html classes don't have to wait at sync data calls to components,
// eg set field when call is done

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  reportWrapper: ReportWrapperModel;
  safetyReport: SafetyReport;
  patient: Patient;

  viewState: string = 'input';

  constructor(@Inject(DOCUMENT) private document: Document, private backendService: BackendService) {
  }

  ngOnInit(): void {
    const reportId = this.getReportIdFromUrl() || "DEMO_ANNA";
    if (reportId === 'DEMO_ANNA') {
      console.log("Using hardcoded report for reportId DEMO_ANNA");
      this.reportWrapper = EXAMPLE_REPORT as ReportWrapperModel;
      this.safetyReport = this.reportWrapper.report.individualCaseSafetyReport.ichicsr.safetyreport;
      this.patient = AppComponent.getPatient(this.safetyReport);
    }
    else {
      console.log("Fetching report from backend for reportId", reportId);
      this.backendService.loadReport(reportId).subscribe(
        (response: ReportWrapperModel) => {
          this.reportWrapper = response;
          this.safetyReport = this.reportWrapper.report.individualCaseSafetyReport.ichicsr.safetyreport;
          this.patient = AppComponent.getPatient(this.safetyReport);
        },
        (err: HttpErrorResponse) => {
          //TODO: handle diferent errors codes separately!
          let errorMessage = "Ett fel inträffade och rapport för rapportId " + reportId + " kunde inte hämtas.\n\n";
          if (err.status === 404) {
            errorMessage = "Ingen rapport med rapportId " + reportId + " hittades.\n\n";
          }
          if (!isNullOrUndefined(err.message)) {
            errorMessage = errorMessage + err.message;
          }
          if (!isNullOrUndefined(err.error)) {
            errorMessage = errorMessage + "\nCaused by: " + err.error.message;
          }
          console.log("Received error response:", errorMessage);
          alert(errorMessage);
        }
      );
    }

    return null;
  }

  private static

  getPatient(report: SafetyReport): Patient {
    return new PatientImpl(report.patient, report.patient.parent.smarttag);
  }

  /**
   * MSIE compatible method for getting the reportId.
   * @returns {string}
   */
  private

  getReportIdFromUrl(): string {
    if (!this.document.location.search) {
      return null;
    }
    const query = this.document.location.search.substring(1);
    const vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split("=");
      if ('reportId' == pair[0]) {
        return pair[1];
      }
    }
  }

  goToPreview() {
    this.viewState = 'preview';
  }

  goToInput() {
    this.viewState = 'input';
  }

  saveReport() {
    console.log("AppComponent.saveReport()");

    //TODO: tmp logging to trouble-shoot data-binding, remove
    console.log("this.safetyReport.patient.drug[0]: ", this.safetyReport.patient.drug[0]);

    if (this.reportWrapper._id === 'DEMO_ANNA') {
      console.log("Report with reportId DEMO_ANNA is only for gui demo purposes and will not be submitted to backend");
      alert("Rapporten för DEMO-ANNA är endast tänkt för demo/test av GUI-fält, och kommer inte att sparas.\n\n" +
        "För att ladda en annan rapport, ange rapportId för denna i url-en, på följande form:\n" +
        "https://sebra-inera.nordicmedtest.se/?reportId=\<rapport-id\>");
    }
    else {
      this.backendService.saveReport(this.reportWrapper).subscribe(() => {
          alert("Rapporten är nu sparad.");
        },
        (err: HttpErrorResponse) => {
          let errorMessage = "Ett fel inträffade och rapporten kunde inte sparas. Försök gärna om en stund igen.\n\n";
          if (!isNull(err.message)) {
            errorMessage = errorMessage + err.message;
          }
          if (!isNull(err.error)) {
            errorMessage = errorMessage + "\nCaused by: " + err.error.message;
          }
          alert(errorMessage);
        }
      )
      ;
    }
  }

  submitReport() {
    console.log("AppComponent.submitReport()");

    if (this.reportWrapper._id === 'DEMO_ANNA') {
      console.log("Report with reportId DEMO_ANNA is only for gui demo purposes and will not be submitted to backend");
      alert("Rapporten för DEMO-ANNA är endast tänkt för demo/test av GUI-fält, och kommer inte att skickas.\n\n" +
        "För att ladda en annan rapport, ange rapportId för denna i url-en, på följande form:\n" +
        "https://sebra-inera.nordicmedtest.se/?reportId=\<rapport-id\>");
    }
    else {

      this.backendService.submitReport(this.reportWrapper).subscribe(() => {
          alert("Rapporten är nu inskickad.\nTack!");
          //TODO: clear form in case of 205 (but not in case of 204)?
          //this.reportWrapper = undefined;
        },
        (err: HttpErrorResponse) => {
          let errorMessage = "Ett fel inträffade och rapporten kunde inte skickas. Försök gärna om en stund igen.\n\n";
          if (!isNull(err.message)) {
            errorMessage = errorMessage + err.message;
          }
          if (!isNull(err.error)) {
            errorMessage = errorMessage + "\nCaused by: " + err.error.message;
          }
          alert(errorMessage);
        }
      )
      ;
    }
  }


//OLD below, for integrationtest:
// reportId = '';
// reportWrapperString = 'No report to show';
// reportWrapper;
// personIdentifier = '<personIdentifier>';
// reportIdFromLastSearch = '';
// static readonly urlPath = '/report/';
// private http: HttpClient;


// onSubmit(biv_form: NgForm) {
//   console.log("Form Submitted");
//   console.log("base url: " + window.location.origin);
//
//   // Make the HTTP request:
//   this.http.get(AppComponent.urlPath + this.reportId.trim()).subscribe
//   ((response: Response) => {
//       console.log("Received successful response");
//       this.reportWrapper = response;
//       //TODO: set to personIdentifier rather than birthdate when available in report
//       this.personIdentifier = this.reportWrapper.report.individualCaseSafetyReport.ichicsr.safetyreport.patient.patientbirthdate;
//       this.reportWrapperString = JSON.stringify(response, null, 2);
//       //}
//     },
//     (err: HttpErrorResponse) => {
//       console.log("Received error response");
//       //TODO: handle diferent errors codes separately? (below should be for 404?)
//       this.reportWrapper = null;
//       this.reportWrapperString = 'No report found';
//       this.personIdentifier = '<personIdentifier>';
//     }
//   );
//
//   this.resetForm(biv_form);
// }
//
// submitReport(biv_form: NgForm) {
//   let jsonReport = JSON.stringify(this.reportWrapper);
//
//   this.http.put(AppComponent.urlPath + this.reportWrapper._id, jsonReport,
//     {
//       headers: new HttpHeaders().set('Content-Type', 'application/json'),
//       params: new HttpParams().set('submit', 'true'),
//     }
//   ).subscribe(() => {
//       alert("Rapporten är nu inskickad.\nTack!");
//       //TODO: clear form in case of 205 (but not in case of 204)?
//       this.reportWrapperString = '';
//       this.reportIdFromLastSearch = '';
//     },
//     (err: HttpErrorResponse) => {
//       let errorMessage = "Ett fel inträffade och rapporten kunde inte skickas. Försök gärna om en stund igen.\n\n";
//       if (!isNull(err.message)) {
//         errorMessage = errorMessage + err.message;
//       }
//       if (!isNull(err.error)) {
//         errorMessage = errorMessage + "\nCaused by: " + err.error.message;
//       }
//       alert(errorMessage);
//     }
//   )
//   ;
// }
//
// resetForm(biv_form: NgForm) {
//   //TODO: reset form in proper way...
//   this.reportIdFromLastSearch = this.reportId;
//   this.reportId = '';
//   biv_form.resetForm(all);
//   biv_form.form.reset();
//   biv_form.reset();
// }

}

