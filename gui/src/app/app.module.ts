import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {DrugsModule} from "./drugs/drugs.module";
import {VigilenceModule} from "./vigilence/vigilence.module";
import {PregnancyModule} from "./pregnancy/pregnancy.module";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {BackendService} from "./backend.service";
import {DiagnosesModule} from "./diagnoses/diagnoses.module";
import {ReporterOpinionModule} from "./reporter-opinion/reporter-opinion.module";
import {SebraSeriousnessModule} from "./seriousness/sebra-seriousness.module";
import {SebraOtherInfoModule} from "./other-info/sebra-other-info.module";
import {InputpageModule} from "./inputpage/inputpage.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    DrugsModule,
    VigilenceModule,
    PregnancyModule,
    NoopAnimationsModule,
    DiagnosesModule,
    ReporterOpinionModule,
    SebraSeriousnessModule,
    SebraOtherInfoModule,
    InputpageModule
  ],

  providers: [BackendService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
