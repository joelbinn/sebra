import {Component, Input, OnInit} from '@angular/core';
import {SafetyReport} from "../model/safety-report.model";

@Component({
  selector: 'sebra-seriousness',
  templateUrl: './seriousness.component.html',
  styleUrls: ['./seriousness.component.css']
})
export class SeriousnessComponent implements OnInit {

  @Input()
  e2bReport: SafetyReport;
  seriousnessOptions: string[] = [];
  dateOfDeath: string;

  constructor() {
  }

  resultsInDeath(selected: boolean) {
    return this.e2bReport.seriousnessdeath = SeriousnessComponent.yesOrNo(selected);
  }

  isLifeThreatening(selected: boolean) {
    this.e2bReport.seriousnesslifethreatening = SeriousnessComponent.yesOrNo(selected);
    return selected;
  }


  private static yesOrNo(selected: boolean): string {
    return selected ? "1" : "2";
  }

  ngOnInit() {

  }

  showInfo() {
    console.debug(this.constructor.name);
  }

}
