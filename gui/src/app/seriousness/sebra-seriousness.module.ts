import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SeriousnessComponent} from './seriousness.component';
import {SebraCommonModule} from "../sebra-common/sebra-common.module";
import {CheckboxModule} from "primeng/primeng";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    SebraCommonModule,
    CheckboxModule,
    FormsModule, // needed for InputTextareaModule
    SebraCommonModule
  ],
  declarations: [SeriousnessComponent],
  exports: [SeriousnessComponent]
})
export class SebraSeriousnessModule { }
