import {Observable} from "rxjs/Observable";
import {ReportWrapperModel} from "./model/report-wrapper.model";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable()
export class BackendService {

  static readonly urlPath = '/report/';

  constructor(private httpClient: HttpClient) {
  }

  //TODO: return tuple object with reportwrapper, patient and drugs?
  loadReport(reportId: string): Observable<ReportWrapperModel> {
    console.log("getting report from backend");
    console.log("base url: " + window.location.origin);
    // TODO: add default error handling
    return this.httpClient.get<ReportWrapperModel>(BackendService.urlPath + reportId);
  }

  saveReport(reportWrapper: ReportWrapperModel): Observable<Object> {
    console.log("BackendService.sendReport()");
    return this.sendReportToBackend(reportWrapper, false);
  }

  submitReport(reportWrapper: ReportWrapperModel): Observable<Object> {
    console.log("BackendService.submitReport()");
    return this.sendReportToBackend(reportWrapper, true);
  }

  sendReportToBackend(reportWrapper: ReportWrapperModel, submitFlag: boolean) {
    console.log("BackendService.sendReportToBackend() - sending report to backend with submit flag " + submitFlag);
    console.log("base url: " + window.location.origin);
    let jsonReport = JSON.stringify(reportWrapper);
    return this.httpClient.put(BackendService.urlPath + reportWrapper._id, jsonReport,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
        params: new HttpParams().set('submit', String(submitFlag)),
      });

  }


}
