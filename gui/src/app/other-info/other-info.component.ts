import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'sebra-other-info',
  templateUrl: './other-info.component.html',
  styleUrls: ['./other-info.component.css']
})
export class OtherInfoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  showInfo() {
    console.debug(this.constructor.name);
  }
}
