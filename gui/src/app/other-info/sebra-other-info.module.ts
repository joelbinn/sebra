import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OtherInfoComponent} from './other-info.component';
import {SebraCommonModule} from "../sebra-common/sebra-common.module";
import {InputTextareaModule} from "primeng/primeng";


@NgModule({
  imports: [
    CommonModule,
    SebraCommonModule,
    InputTextareaModule
  ],
  declarations: [OtherInfoComponent],
  exports: [OtherInfoComponent]
})
export class SebraOtherInfoModule {
}
