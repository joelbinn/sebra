import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {InputpageComponent} from "./inputpage.component";
import {DrugsModule} from "../drugs/drugs.module";
import {VigilenceModule} from "../vigilence/vigilence.module";
import {PregnancyModule} from "../pregnancy/pregnancy.module";
import {DiagnosesModule} from "../diagnoses/diagnoses.module";
import {ReporterOpinionModule} from "../reporter-opinion/reporter-opinion.module";
import {SebraSeriousnessModule} from "../seriousness/sebra-seriousness.module";
import {SebraOtherInfoModule} from "../other-info/sebra-other-info.module";

@NgModule({
  declarations: [
    InputpageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    DrugsModule,
    VigilenceModule,
    PregnancyModule,
    DiagnosesModule,
    ReporterOpinionModule,
    SebraSeriousnessModule,
    SebraOtherInfoModule,
  ],
  exports: [
    InputpageComponent
  ],
})
export class InputpageModule {
}
