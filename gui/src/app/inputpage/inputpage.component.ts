import {Component, Input, OnInit} from '@angular/core';
import {Patient} from "../model/patient.model";
import {DrugList} from "../model/druglist.model";
import {Reaction} from "../model/reaction.model";
import {Diagnosis} from "../diagnoses/diagnosis.model";
import {SafetyReport} from "../model/safety-report.model";
import {DrugListImpl} from "../model/druglist-impl.model";
import {ReactionsImpl} from "../model/reactions-impl.model";
import {Diagnoses} from "../diagnoses/diagnoses.model";

@Component({
  selector: 'inputpage',
  templateUrl: './inputpage.component.html',
})

export class InputpageComponent implements OnInit {

  @Input()
  patient: Patient;

  druglist: DrugList;
  reactionList: Reaction[];
  diagnoses: Diagnosis[];
  reporterOpinion: string;

  @Input()
  safetyReport: SafetyReport;

  ngOnInit(): void {
    this.druglist = InputpageComponent.getDrugs(this.safetyReport);
    this.reactionList = InputpageComponent.getReactions(this.safetyReport);
    this.diagnoses = InputpageComponent.getDiagnoses(this.safetyReport);
    this.reporterOpinion = InputpageComponent.getReporterOpinion(this.safetyReport);
  }

  private static getDrugs(report: SafetyReport): DrugList {
    return new DrugListImpl(report.patient.drug, report.patient.parent.smarttag.drug);
  }

  private static getReactions(report: SafetyReport): Reaction[] {
    let reactions = new ReactionsImpl(report.patient.reaction);
    return reactions.list;
  }

  private static getDiagnoses(report: SafetyReport): Diagnosis[] {
    const diagnoses = new Diagnoses(report.patient.medicalhistoryepisode);
    return diagnoses.list;
  }

  private static getReporterOpinion(report: SafetyReport): string {
    return report.patient.summary.narrativeincludeclinical;
  }


}

