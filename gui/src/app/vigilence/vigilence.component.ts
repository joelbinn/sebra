import {Component, Input} from '@angular/core';
import {SelectItem} from "primeng/primeng";
import {Reaction} from "../model/reaction.model";

@Component({
  selector: 'sebra-vigilence',
  templateUrl: './vigilence.component.html',
})

export class VigilenceComponent {
  filteredReactionOptions: SelectItem[];

  @Input()
  listOfReactions: Reaction[];

  readonly availableReactionOptions: SelectItem[] = [
    {label: "R23.1 Blekhet", value: "R23.1"},
    {label: "R22.3 Lokaliserad svullnad eller knöl i övre extremitet", value: "R22.3"},
    {label: "R30.1 Tenesmer i urinblåsan", value: "R30.1"}
    // etc..
  ];

  readonly availableOutcomes: SelectItem[] = [
    // Giltiga värden 1-6, ej 2
    {value: 1, label: "Tillfrisknat utan men"},
    {value: 3, label: "Ännu ej tillfrisknat"},
    {value: 4, label: "Tillfrisknat med men"},
    {value: 5, label: "Patienten avled"},
    {value: 6, label: "Okänt"}
  ];

  findPrimarySourceReaction(event: any) {
    this.filteredReactionOptions = this.availableReactionOptions.filter(selectItem => selectItem.label.toLowerCase().indexOf(event.query.toLowerCase()) >= 0);
  }

  //TODO: add show info also on hover, not just click (in all components)
  showInfo() {
    console.debug('Show info on Vigilence component');
  }
}
