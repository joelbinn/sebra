import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {VigilenceComponent} from './vigilence.component';
import {SebraCommonModule} from "../sebra-common/sebra-common.module";
import {AutoCompleteModule, DropdownModule} from 'primeng/primeng';
import {RadioButtonModule} from 'primeng/primeng';

@NgModule({
  declarations: [
    VigilenceComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    SebraCommonModule,
    DropdownModule,
    AutoCompleteModule,
    RadioButtonModule
  ],
  exports: [
    VigilenceComponent
  ],

  providers: [],
})
export class VigilenceModule {
}
