export interface Patient {
  readonly gender: string;

  isFemale(): boolean;

  readonly firstName: string;

  readonly lastName: string;

  readonly age: number;

  readonly identityNumber: string;
}

