export class StringDate {
  _date: Date;

  constructor(private _datestring: string, private _datestringformat: string = "102") {
    //custom format 102=CCYYMMDD
    //iso-format: YYYY-MM-DD
    //TODO: use lib date-and-time instead (npm install --save date-and-time)
    const year = Number(_datestring.substr(0, 4));
    const month: number = (Number(_datestring.substr(4, 2))) - 1;
    const day = Number(_datestring.substr(6, 2));
    this._date = new Date(year, month, day);
    console.log("parsing datestring " + _datestring + " to date " + this._date);
  }

  set date(date: Date) {
    this._date = date;
    //TODO: this._datestringformat = getFormatFromDate(date: Date);
    this._datestring = this.parse(this._datestringformat, date);
  }

  get date() {
    return this._date;
  }

  get datestring() {
    return this._datestring;
  }

  private parse(format: string, date: Date): string {
    //TODO: parse based on format!! if different formats are needed, Malin checks
    return this._date.toISOString();
  }
}
