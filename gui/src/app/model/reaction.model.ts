export interface Reaction {
  readonly primarysourcereaction: string;
  termhighlighted: number;
  reactionstartdateformat: number;
  reactionstartdate: number;
  reactionoutcome: number;
}
