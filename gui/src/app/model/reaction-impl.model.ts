import {Reaction} from "./reaction.model";

export class ReactionImpl implements Reaction {

  primarysourcereaction: string;
  termhighlighted: number;
  reactionstartdateformat: number;
  reactionstartdate: number;
  reactionoutcome: number;


  withPrimarySourceReaction(value: string) {
    this.primarysourcereaction = value;
    return this;
  }

  withTermHighlighted(value: number) {
    this.termhighlighted = value;
    return this;
  }

  withReactionStartDateformat(value: number) {
    this.reactionstartdateformat = value;
    return this;
  }

  withReactionStartDate(value: number) {
    this.reactionstartdate = value;
    return this;
  }

  withReactionOutcome(value: number) {
    this.reactionoutcome = value;
    return this;
  }
}
