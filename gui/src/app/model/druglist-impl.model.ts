import {DrugList} from "./druglist.model";
import {DrugImpl} from "./drug-impl.model";
import {Drug} from "./drug.model";
import * as _ from "lodash";

export class DrugListImpl implements DrugList {
  constructor(private orig_drugs: any[], private drugs_extra_info: any[]) {
  }

  get drugs() {
    return this.orig_drugs.map(orig_drug => {
      return new DrugImpl(orig_drug,
        this.drugs_extra_info.find(info => info.drugname === orig_drug.medicinalproduct))
    });
  }

  add(drug: any): void {
    this.orig_drugs.push({medicinalproduct: drug.medicinalproduct, drugdosageform: drug.drugdosageform});
    this.drugs_extra_info.push({drugname: drug.medicinalproduct, intensity: drug.intensity});
  }

  remove(drug: Drug): void {
    _.remove(this.orig_drugs, (element) => element.medicinalproduct === drug.medicinalproduct);
    _.remove(this.drugs_extra_info, (element) => element.drugname === drug.medicinalproduct);
  }

}
