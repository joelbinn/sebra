import {Patient} from "./patient.model";

export class PatientImpl implements Patient {

  constructor(private patient: any, private smarttag: any) {
  }

  get gender(): string {
    return this.patient.patientsex === "2" ? "Kvinna" : "Man";
  }

  //TODO: handle nullpointer in below methods?
  //TODO: move to patient component / class?
  isFemale(): boolean {
    return this.patient.patientsex === "2";
  }

  get firstName() {
    return this.smarttag.patientfirstname;
  }

  get lastName() {
    return this.smarttag.patientlastname;
  }

  get age() {
    return this.getYearsFromDate(this.patient.patientbirthdate);
  }

  get identityNumber() {
    return this.smarttag.patientidentitynumber;
  }

  //TODO: use eg https://github.com/urish/angular-moment instead?
  private getYearsFromDate(birthdate: string): number {
    const currentYear = new Date().getFullYear();
    const birthyear = parseInt(birthdate.substring(0, 4));
    return currentYear - birthyear;
  }

}
