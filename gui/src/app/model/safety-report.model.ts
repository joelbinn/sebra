/**
 * A model for representing JSON report.individualCaseSafetyReport.ichicsr.safetyreport
 */
export class SafetyReport {
  safetyreportversion: string;
  safetyreportid: string;
  primarysourcecountry: string;
  occurcountry: string;
  transmissiondateformat: string;
  transmissiondate: string;
  reporttype: string;
  serious: string;
  seriousnessdeath: string;
  seriousnesslifethreatening: string;
  seriousnesshospitalization: string;
  seriousnessdisabling: string;
  seriousnesscongenitalanomali: string;
  seriousnessother: string;
  receivedateformat: string;
  receivedate: string;
  receiptdateformat: string;
  receiptdate: string;
  companynumb: string;
  primarysource: any; //PrimarysourceType
  sender: any; //SenderType
  receiver: any; //ReceiverType
  patient: any; //PatientType
  any: any[];
}
