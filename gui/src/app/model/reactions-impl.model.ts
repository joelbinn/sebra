import {Reactions} from "./reactions.model";
import {Reaction} from "./reaction.model";
import {ReactionImpl} from "./reaction-impl.model";

export class ReactionsImpl implements Reactions {

  list: Reaction[];

  constructor(listOfReactions: Array<any>) {
    this.list = listOfReactions.map(reaction => {
      return new ReactionImpl()
        .withPrimarySourceReaction(reaction.primarysourcereaction)
        .withTermHighlighted(reaction.termhighlighted)
        .withReactionStartDateformat(reaction.reactionstartdateformat)
        .withReactionStartDate(reaction.reactionstartdate)
        .withReactionOutcome(reaction.reactionoutcome)
        ;
    });
  }

}
