export interface Drug {
  // report.individualCaseSafetyReport.ichicsr.safetyreport.patient.drug.[i]
  drugcharacterization: string;
  medicinalproduct: string;
  drugbatchnumb: string;
  drugauthorizationnumb: string;
  drugauthorizationhold: string;
  drugdosagetext: string;
  drugdosageform: string;
  drugadministrationroute: string;
  drugstartdateformat: string;
  drugstartdate: string;
  drugenddateformat: string;
  drugenddate: string;
  activesubstance: {
    activesubstancename: string;
  }

  // Additional info collected from s.k "Smart tags" found in...
  // report.individualCaseSafetyReport.ichicsr.safetyreport.patient.parent.parentmedicalrelevanttext.drug.[i]
  drugname: string;
  intensity: string;
  indication: string;
  reactdiscontinue: string;
  reactreturned: string;
  nplpackid: string;
  nplid: string;
}
