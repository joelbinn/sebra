import {Drug} from "./drug.model";

export class DrugImpl implements Drug {

  constructor(private drug: any, private parentmedicalrelevanttext: any) {
  }

  // report.individualCaseSafetyReport.ichicsr.safetyreport.patient.drug.[i]
  //TODO: if any logic is needed at get/set, implement getters/setters (get <prop name>, set <pro name>)
  drugcharacterization: string = this.drug.drugcharacterization;
  medicinalproduct: string = this.drug.medicinalproduct;
  drugbatchnumb: string = this.drug.drugbatchnumb;
  drugauthorizationnumb: string = this.drug.drugauthorizationnumb;
  drugauthorizationhold: string = this.drug.drugauthorizationhold;
  drugdosagetext: string = this.drug.drugdosagetext;
  drugdosageform: string = this.drug.drugdosageform;
  drugadministrationroute: string = this.drug.drugadministrationroute;
  drugstartdateformat: string = this.drug.drugstartdateformat;
  drugstartdate: string = this.drug.drugstartdate;
  drugenddateformat: string = this.drug.drugenddateformat;
  drugenddate: string = this.drug.drugenddate;
  activesubstance: {
    activesubstancename: string;
  } = this.drug.activesubstance && this.drug.activesubstance.activesubstancename || undefined;

  // report.individualCaseSafetyReport.ichicsr.safetyreport.patient.parent.parentmedicalrelevanttext.drug.[i]
  drugname: string = this.parentmedicalrelevanttext.drugname;
  intensity: string = this.parentmedicalrelevanttext.intensity;
  indication: string = this.parentmedicalrelevanttext.indication;
  reactdiscontinue: string = this.parentmedicalrelevanttext.reactdiscontinue;
  reactreturned: string = this.parentmedicalrelevanttext.reactreturned;
  nplpackid: string = this.parentmedicalrelevanttext.nplpackid;
  nplid: string = this.parentmedicalrelevanttext.nplid;

  //utility fields
  drugstartdate_as_date: Date;

}
