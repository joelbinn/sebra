import {Drug} from "./drug.model";

export interface DrugList {

  drugs: Drug[];

  add(value: any): void;

  remove(drug: Drug): void;


}
