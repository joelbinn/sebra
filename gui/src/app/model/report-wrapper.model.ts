export interface ReportWrapperModel {
  _id: string;
  report: any;
  status: any;
}

// export interface Patient {
//   [propname: string]: any;
// }

export const EXAMPLE_REPORT =
  {
    "report": {
      "individualCaseSafetyReport": {
        "ichicsr": {
          "ichicsrmessageheader": {
            "messagetype": "ichicsr",
            "messageformatversion": "2.1",
            "messageformatrelease": "2.0",
            "messagenumb": "SE-SEBRA-68",
            "messagesenderidentifier": "SEBRA",
            "messagereceiveridentifier": "SEMPAT",
            "messagedateformat": "204",
            "messagedate": "20140710153001",
            "any": []
          },
          "safetyreport": {
            "safetyreportversion": "0",
            "safetyreportid": "SE-SEBRA-68",
            "primarysourcecountry": "SE",
            "occurcountry": "SE",
            "transmissiondateformat": "102",
            "transmissiondate": "20140710",
            "reporttype": "1",
            "serious": "1",
            "seriousnessdeath": "1",
            "seriousnesslifethreatening": "2",
            "seriousnesshospitalization": "2",
            "seriousnessdisabling": "2",
            "seriousnesscongenitalanomali": "2",
            "seriousnessother": "2",
            "receivedateformat": "102",
            "receivedate": "20140710",
            "receiptdateformat": "102",
            "receiptdate": "20140710",
            "companynumb": "SE-SEBRA-68",
            "primarysource": {
              "reportertitle": "Läkare, specialist i allmänmedicin",
              "reportergivename": "Topi",
              "reporterfamilyname": "Laajoki",
              "reporterorganization": "Knivsta Vårdcentral",
              "reporterdepartment": "Avdelning 1",
              "reporterstreet": "Ängbyvägen 8",
              "reportercity": "Knivsta",
              "reporterstate": "C",
              "reporterpostcode": "741 30",
              "reportercountry": "SE",
              "qualification": "1",
              "any": []
            },
            "sender": {
              "sendertype": "3",
              "senderorganization": "ARTIS",
              "senderdepartment": "SRQ",
              "sendercountrycode": "SE",
              "any": [],
              "lang": "se"
            },
            "receiver": {
              "receivertype": "2",
              "receiverorganization": "SEMPAT",
              "receiverdepartment": "Department of Pharmacovigilance",
              "receivercountrycode": "SE",
              "any": []
            },
            "patient": {
              "patientbirthdateformat": "102",
              "patientbirthdate": "19641212",
              "patientonsetage": "49",
              "patientonsetageunit": "801",
              "gestationperiod": "5",
              "gestationperiodunit": "802",
              "patientsex": "2",
              "patientmedicalhistorytext": "Vid 30-årsålder lätta magbesvär i samband med en penicillinkur. För\n 5 år sedan lätta erytem i samband med heracillinkur för sårinfektion.\n ",
              "medicalhistoryepisode": [
                {
                  "patientepisodename": "Förmaksflimer",
                  "patientmedicalstartdateformat": "610",
                  "patientmedicalstartdate": "201403",
                  "any": []
                },
                {
                  "patientepisodename": "Kronisk hjärtinsufficiens",
                  "patientmedicalstartdateformat": "602",
                  "patientmedicalstartdate": "2014",
                  "any": []
                }
              ],
              "patientdeath": {
                "patientdeathdateformat": "102",
                "patientdeathdate": "20140601",
                "patientdeathcause": {
                  "patientdeathreport": "Patienten blev överkörd av en buss.\n ",
                  "any": []
                },
                "any": []
              },
              "parent": {
                "smarttag": {
                  "patientfirstname": "DEMO-ANNA",
                  "patientlastname": "Svensson",
                  "patientidentitynumber": "196412121212",
                  "reporterhsaid": "0123456789",
                  "reporterunithsaid": "0123456789012",
                  "healthcarecareserviceprovider": "0123456789013",
                  "reporterphone": "0701234567",
                  "municipalitycode": "0330",
                  "countycode": "03",
                  "hsatitle": "LK",
                  "caretype": "02",
                  "management": "Landsting/Region",
                  "drug": [
                    {
                      "drugname": "Simvastatin Amneal",
                      "intensity": "40 mg",
                      "indication": "Sänker blodfetter",
                      "reactdiscontinue": "Ja",
                      "reactreturned": "Ja",
                      "nplpackid": "20140512001234",
                      "nplid": "20140512000234"
                    },
                    {
                      "drugname": "Kåvepenin®",
                      "intensity": "500 mg",
                      "indication": "Mot infektion",
                      "reactdiscontinue": "Nej",
                      "reactreturned": "Ja"
                    },
                    {
                      "drugname": "Humira®",
                      "intensity": "40 mg/0,8 ml",
                      "indication": "Mot RA"
                    }
                  ],
                  "document": {
                    "type": "Rapport pdf",
                    "name": "SE-SRQ-nummer.pdf"
                  }
                },
                "any": []
              },
              "reaction": [
                {
                  "primarysourcereaction": "Muskelvärk",
                  "termhighlighted": "2",
                  "reactionstartdateformat": "102",
                  "reactionstartdate": "20140401",
                  "reactionoutcome": "1",
                  "any": []
                },
                {
                  "primarysourcereaction": "Hudutslag",
                  "termhighlighted": "2",
                  "reactionstartdateformat": "102",
                  "reactionstartdate": "20140526",
                  "reactionoutcome": "5",
                  "any": []
                },
                {
                  "primarysourcereaction": "Feber",
                  "termhighlighted": "2",
                  "reactionstartdateformat": "102",
                  "reactionstartdate": "20140402",
                  "reactionoutcome": "1",
                  "any": []
                }
              ],
              "drug": [
                {
                  "drugcharacterization": "1",
                  "medicinalproduct": "Simvastatin Amneal",
                  "drugbatchnumb": "12312123",
                  "drugauthorizationnumb": "43257",
                  "drugauthorizationholder": "Amneal Pharma Europe Limited\n ",
                  "drugdosagetext": "1 x 2",
                  "drugdosageform": "Filmdragerad tablett",
                  "drugadministrationroute": "001",
                  "drugstartdateformat": "102",
                  "drugstartdate": "20140202",
                  "drugenddateformat": "102",
                  "drugenddate": "20140414",
                  "activesubstance": {
                    "activesubstancename": "simvastatin",
                    "any": []
                  },
                  "any": []
                },
                {
                  "drugcharacterization": "1",
                  "medicinalproduct": "Kåvepenin®",
                  "drugbatchnumb": null,
                  "drugauthorizationnumb": "084090",
                  "drugauthorizationholder": "Meda",
                  "drugdosagetext": "2 x 2",
                  "drugdosageform": "Filmdragerad tablett",
                  "drugadministrationroute": null,
                  "drugstartdateformat": "102",
                  "drugstartdate": "20140525",
                  "drugenddateformat": "102",
                  "drugenddate": "20140610",
                  "activesubstance": {
                    "activesubstancename": "fenoximetylpenicillinkalium",
                    "any": []
                  },
                  "any": []
                },
                {
                  "drugcharacterization": "2",
                  "medicinalproduct": "Humira®",
                  "drugbatchnumb": "9876543210",
                  "drugauthorizationnumb": "18474",
                  "drugauthorizationholder": "AbbVie Ltd",
                  "drugdosagetext": "1 x 1",
                  "drugdosageform": "Injektionsvätska, lösning",
                  "drugadministrationroute": null,
                  "drugstartdateformat": "102",
                  "drugstartdate": "20130602",
                  "drugenddateformat": null,
                  "drugenddate": null,
                  "activesubstance": {
                    "activesubstancename": "adalimumab",
                    "any": []
                  },
                  "any": []
                }
              ],
              "summary": {
                "narrativeincludeclinical": "49-årig kvinna som behandlas sedan ett år med Humira med god\n effekt. För 3 månader sedan insatt Simvastatin men har själv slutat att ta den pga.\n besvärlig muskelvärk. Inte tagit Simvastatin den senaste månaden. Nu under pågående\n Kåvepeninbehandling mot halsfluss med start 26/5 har patienten fått utspridda illröda erytem\n på överarmar och lår. Misstänks vara penicillinorsakade. Enligt min bedömning har patienten\n fått två biverkningar: muskelvärk av Simvastatin och erytem av Kåvepenin.\n ",
                "reportercomment": "Bor på Teneriffa på vinterhalvåret. Patientens pappa hade hemofili A utan\n komplikationer. Telefonnr: 018-470034\n ",
                "any": []
              },
              "any": []
            },
            "any": []
          },
          "any": [],
          "lang": "se"
        },
        "any": []
      },
      "any": []
    },
    "status": "DRAFT",
    "_id": "DEMO_ANNA"
  };
