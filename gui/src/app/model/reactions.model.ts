import {Reaction} from "./reaction.model";

export interface Reactions {
  list: Reaction[];
}

