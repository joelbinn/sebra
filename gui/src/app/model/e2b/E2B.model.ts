export interface SafetyReportType {
  safetyreportversion: string;
  safetyreportid: string;
  primarysourcecountry: string;
  occurcountry: string;
  transmissiondateformat: string;
  transmissiondate: string;
  reporttype: string;
  serious: string;
  seriousnessdeath: string;
  seriousnesslifethreatening: string;
  seriousnesshospitalization: string;
  seriousnessdisabling: string;
  seriousnesscongenitalanomali: string;
  seriousnessother: string;
  receivedateformat: string;
  receivedate: string;
  receiptdateformat: string;
  receiptdate: string;
  companynumb: string;
  primarysource: PrimarysourceType;
  sender: SenderType;
  receiver: ReceiverType;
  patient: PatientType;
}

export interface PrimarysourceType {
  reportertitle: string;
  reportergivename: string;
  reporterfamilyname: string;
  reporterorganization: string;
  reporterdepartment: string;
  reporterstreet: string;
  reportercity: string;
  reporterstate: string;
  reporterpostcode: string;
  reportercountry: string;
  qualification: string;
}

export interface SenderType {
  sendertype: string;
  senderorganization: string;
  senderdepartment: string;
  sendercountrycode: string;
  lang: string;
}

export interface ReceiverType {
  receivertype: string;
  receiverorganization: TypeOfMessageReceiverIdentifierEnum;
  receiverdepartment: string;
  receivercountrycode: string;
}

export interface PatientType {
  patientbirthdateformat: string;
  patientbirthdate: string;
  patientonsetage: string;
  patientonsetageunit: string;
  gestationperiod: string;
  gestationperiodunit: string;
  patientsex: string;
  patientmedicalhistorytext: string;
  medicalhistoryepisode: MedicalHistoryEpisodeType[];
  patientdeath: PatientDeathType;
  parent: ParentType;
  reaction: ReactionType[];
  drug: DrugType[];
  summary: SummaryType;
}

export interface MedicalHistoryEpisodeType {
  patientepisodename: string;
  patientmedicalstartdateformat: string;
  patientmedicalstartdate: string;
}

export interface PatientDeathType {
  patientdeathdateformat: string;
  patientdeathdate: string;
  patientdeathcause: PatientDeathCauseType;
}

export interface ParentType {
  smarttag: SmartTag;
}

export interface SmartTag {
  patientfirstname: string;
  patientlastname: string;
  patientidentitynumber: string;
  reporterhsaid: string;
  reporterunithsaid: string;
  healthcarecareserviceprovider: string;
  reporterphone: string;
  municipalitycode: string;
  countycode: string;
  hsatitle: string;
  caretype: string;
  management: string;
  drug: Sebra.DrugType[];
  document: Sebra.DocumentType[];
}

export interface ReactionType {
  primarysourcereaction: string;
  termhighlighted: string;
  reactionstartdateformat: string;
  reactionstartdate: string;
  reactionoutcome: string;
}

export interface DrugType {
  drugcharacterization: string;
  medicinalproduct: string;
  drugbatchnumb: string;
  drugauthorizationnumb: string;
  drugauthorizationholder: string;
  drugdosagetext: string;
  drugdosageform: string;
  drugadministrationroute: string;
  drugstartdateformat: string;
  drugstartdate: string;
  drugenddateformat: string;
  drugenddate: string;
  activesubstance: ActiveSubstanceType;
}

export interface SummaryType {
  narrativeincludeclinical: string;
  reportercomment: string;
}

export interface PatientDeathCauseType {
  patientdeathreport: string;
}

export interface ActiveSubstanceType {
  activesubstancename: string;
}


namespace Sebra {

  export interface DrugType {
    drugname: string;
    intensity: string;
    indication: string;
    reactdiscontinued: string;
    reactreturned: string;
    nplpackid: string;
    nplid: string;
  }

  export interface DocumentType {
    type: string;
    name: string;
  }
}

type TypeOfMessageReceiverIdentifierEnum = "SEMPA" | "SEMPAT";
