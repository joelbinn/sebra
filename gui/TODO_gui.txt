- data-binding; ändringar som görs i GUI-t måste propageras till fälten i AppComponent... behöver vi lägga till
events från sub-komponenterna..?


- datum-fälten, hantera olika format, inkl ta bort datepicker och ha annan format-hjälp och validering?
  skapa egen komponent som tillåter flera format, och översätter till önskat format? inkl sätter format-fältet korrekt
- graviditet, antal-rutan, fixa så att "antal"-texten inte lägger sig över ifyllt värde?
- förhandsgranska-sidan
- spara-knappen inaktiv om inga ändringar
- knappar in-aktiva om inte input-fälten validerar, mha service?
(- vidare-knappen inaktiv om inte all obligatorisk data är ifylld - följer av require-validerings-fel, men dubbelkolla)
- jounalsystems-mock; sida som anropar Sebra-applikationen på motsvarande sätt som det sedan är tänkt att journal-systemen ska göra;
i en första version: lista existerande DRAFT-rapporter, som klickbara länkar, om man klickar på dem öppnas sebra med motsvarande rapport
i en andra version: kunna skapa en rapport, tex via en knapp "Skapa rapport", då ska "createDraft"-logik köras i backend i sebra,
och man ska få tillbaka ett rapportid för den skapade rapporten. Man ska sedan kunna öppna sebra m detta rapport-id.
- Styling
(inkl hantera "h1-whitespace" på bättre sätt,
under "Allvarlighetsgrad", inte fetstilt)
- navigering mellan sidor, gå över till ang-routes?


- visa lämplig info-text vid click/hover på resp info-symbol
