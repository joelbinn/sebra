def targetServer = 'sebra.inera.nordicmedtest.se'
def sshCommand = "ssh inera@${targetServer} -p 14022"
def remoteAppStatus = 'UNKNOWN';

pipeline {
    agent any

    stages {
        stage('GetSourceCode') {
            steps {
                git url: 'https://bitbucket.org/ineraservices/sebra.git'
            }
        }

        stage('Build') {
            steps {
                sh 'chmod 775 gradlew'
                sh './gradlew clean build'
            }
        }

        stage('Deploy') {
            steps {
                script {
                    sshagent(credentials: ['sebra_deploy_key']) {

                        echo "Target server is ${targetServer}"

                        remoteAppStatus = sh(
                                script: "${sshCommand} test -e /var/run/sebra/sebra.pid && echo 'Running' || echo 'na'",
                                returnStdout: true
                        ).trim()

                        if (remoteAppStatus == 'Running') {
                            echo 'Stopping application...'
                            sh "${sshCommand} /usr/sbin/service sebra stop"
                        } else {
                            echo "WARNING: No SEBRA application is running on target server."
                        }

                        echo "Uploading jar to target server ..."
                        sh 'scp -P 14022 build/libs/sebra-0.0.2-SNAPSHOT.jar inera@sebra.inera.nordicmedtest.se:/home/inera/sebra-deploy'
                        echo 'Starting application...'
                        sh "${sshCommand} /usr/sbin/service sebra start"
                    }
                }
            }
        }
    }
    post {
        failure {
            mail to: 'ann-sofie.stolperud@cag.se firas.adiler@cag.se',
                    subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                    body: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':\nCheck console output at ${env.BUILD_URL}"
        }
    }
}
