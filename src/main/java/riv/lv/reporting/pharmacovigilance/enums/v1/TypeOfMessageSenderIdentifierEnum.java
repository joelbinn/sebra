
package riv.lv.reporting.pharmacovigilance.enums.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeOfMessageSenderIdentifierEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeOfMessageSenderIdentifierEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SEBRAT"/&gt;
 *     &lt;enumeration value="SEBRA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypeOfMessageSenderIdentifierEnum", namespace = "urn:riv:lv:reporting:pharmacovigilance:enums:1")
@XmlEnum
public enum TypeOfMessageSenderIdentifierEnum {

    SEBRAT,
    SEBRA;

    public String value() {
        return name();
    }

    public static TypeOfMessageSenderIdentifierEnum fromValue(String v) {
        return valueOf(v);
    }

}
