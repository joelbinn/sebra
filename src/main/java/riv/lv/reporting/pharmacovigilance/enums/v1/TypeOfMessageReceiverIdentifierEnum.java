
package riv.lv.reporting.pharmacovigilance.enums.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeOfMessageReceiverIdentifierEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeOfMessageReceiverIdentifierEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SEMPA"/&gt;
 *     &lt;enumeration value="SEMPAT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypeOfMessageReceiverIdentifierEnum", namespace = "urn:riv:lv:reporting:pharmacovigilance:enums:1")
@XmlEnum
public enum TypeOfMessageReceiverIdentifierEnum {

    SEMPA,
    SEMPAT;

    public String value() {
        return name();
    }

    public static TypeOfMessageReceiverIdentifierEnum fromValue(String v) {
        return valueOf(v);
    }

}
