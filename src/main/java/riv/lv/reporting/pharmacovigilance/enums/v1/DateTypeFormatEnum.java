
package riv.lv.reporting.pharmacovigilance.enums.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DateTypeFormatEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DateTypeFormatEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="YYYY"/&gt;
 *     &lt;enumeration value="YYYYMM"/&gt;
 *     &lt;enumeration value="YYYYMMDD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DateTypeFormatEnum", namespace = "urn:riv:lv:reporting:pharmacovigilance:enums:1")
@XmlEnum
public enum DateTypeFormatEnum {

    YYYY,
    YYYYMM,
    YYYYMMDD;

    public String value() {
        return name();
    }

    public static DateTypeFormatEnum fromValue(String v) {
        return valueOf(v);
    }

}
