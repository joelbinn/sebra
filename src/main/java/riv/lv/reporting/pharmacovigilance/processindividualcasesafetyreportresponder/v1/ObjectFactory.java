
package riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProcessIndividualCaseSafetyReport_QNAME = new QName("urn:riv:lv:reporting:pharmacovigilance:ProcessIndividualCaseSafetyReportResponder:1", "ProcessIndividualCaseSafetyReport");
    private final static QName _ProcessIndividualCaseSafetyReportResponse_QNAME = new QName("urn:riv:lv:reporting:pharmacovigilance:ProcessIndividualCaseSafetyReportResponder:1", "ProcessIndividualCaseSafetyReportResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessIndividualCaseSafetyReportType }
     * 
     */
    public ProcessIndividualCaseSafetyReportType createProcessIndividualCaseSafetyReportType() {
        return new ProcessIndividualCaseSafetyReportType();
    }

    /**
     * Create an instance of {@link ProcessIndividualCaseSafetyReportResponseType }
     * 
     */
    public ProcessIndividualCaseSafetyReportResponseType createProcessIndividualCaseSafetyReportResponseType() {
        return new ProcessIndividualCaseSafetyReportResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessIndividualCaseSafetyReportType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:lv:reporting:pharmacovigilance:ProcessIndividualCaseSafetyReportResponder:1", name = "ProcessIndividualCaseSafetyReport")
    public JAXBElement<ProcessIndividualCaseSafetyReportType> createProcessIndividualCaseSafetyReport(ProcessIndividualCaseSafetyReportType value) {
        return new JAXBElement<ProcessIndividualCaseSafetyReportType>(_ProcessIndividualCaseSafetyReport_QNAME, ProcessIndividualCaseSafetyReportType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessIndividualCaseSafetyReportResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:lv:reporting:pharmacovigilance:ProcessIndividualCaseSafetyReportResponder:1", name = "ProcessIndividualCaseSafetyReportResponse")
    public JAXBElement<ProcessIndividualCaseSafetyReportResponseType> createProcessIndividualCaseSafetyReportResponse(ProcessIndividualCaseSafetyReportResponseType value) {
        return new JAXBElement<ProcessIndividualCaseSafetyReportResponseType>(_ProcessIndividualCaseSafetyReportResponse_QNAME, ProcessIndividualCaseSafetyReportResponseType.class, null, value);
    }

}
