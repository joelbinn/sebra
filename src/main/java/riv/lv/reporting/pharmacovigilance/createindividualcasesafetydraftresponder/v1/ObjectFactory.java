
package riv.lv.reporting.pharmacovigilance.createindividualcasesafetydraftresponder.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the riv.lv.reporting.pharmacovigilance.createindividualcasesafetydraftresponder.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateIndividualCaseSafetyDraft_QNAME = new QName("urn:riv:lv:reporting:pharmacovigilance:CreateIndividualCaseSafetyDraftResponder:1", "CreateIndividualCaseSafetyDraft");
    private final static QName _CreateIndividualCaseSafetyDraftResponse_QNAME = new QName("urn:riv:lv:reporting:pharmacovigilance:CreateIndividualCaseSafetyDraftResponder:1", "CreateIndividualCaseSafetyDraftResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: riv.lv.reporting.pharmacovigilance.createindividualcasesafetydraftresponder.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateIndividualCaseSafetyDraftType }
     * 
     */
    public CreateIndividualCaseSafetyDraftType createCreateIndividualCaseSafetyDraftType() {
        return new CreateIndividualCaseSafetyDraftType();
    }

    /**
     * Create an instance of {@link CreateIndividualCaseSafetyDraftResponseType }
     * 
     */
    public CreateIndividualCaseSafetyDraftResponseType createCreateIndividualCaseSafetyDraftResponseType() {
        return new CreateIndividualCaseSafetyDraftResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateIndividualCaseSafetyDraftType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:lv:reporting:pharmacovigilance:CreateIndividualCaseSafetyDraftResponder:1", name = "CreateIndividualCaseSafetyDraft")
    public JAXBElement<CreateIndividualCaseSafetyDraftType> createCreateIndividualCaseSafetyDraft(CreateIndividualCaseSafetyDraftType value) {
        return new JAXBElement<CreateIndividualCaseSafetyDraftType>(_CreateIndividualCaseSafetyDraft_QNAME, CreateIndividualCaseSafetyDraftType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateIndividualCaseSafetyDraftResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:lv:reporting:pharmacovigilance:CreateIndividualCaseSafetyDraftResponder:1", name = "CreateIndividualCaseSafetyDraftResponse")
    public JAXBElement<CreateIndividualCaseSafetyDraftResponseType> createCreateIndividualCaseSafetyDraftResponse(CreateIndividualCaseSafetyDraftResponseType value) {
        return new JAXBElement<CreateIndividualCaseSafetyDraftResponseType>(_CreateIndividualCaseSafetyDraftResponse_QNAME, CreateIndividualCaseSafetyDraftResponseType.class, null, value);
    }

}
