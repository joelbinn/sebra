
package riv.lv.reporting.pharmacovigilance.createindividualcasesafetydraftresponder.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;
import riv.lv.reporting.pharmacovigilance.v1.IndividualCaseSafetyDraftType;


/**
 * <p>Java class for CreateIndividualCaseSafetyDraftType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateIndividualCaseSafetyDraftType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="individualCaseSafetyDraft" type="{urn:riv:lv:reporting:pharmacovigilance:1}IndividualCaseSafetyDraftType"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateIndividualCaseSafetyDraftType", propOrder = {
    "individualCaseSafetyDraft",
    "any"
})
public class CreateIndividualCaseSafetyDraftType {

    @XmlElement(required = true)
    protected IndividualCaseSafetyDraftType individualCaseSafetyDraft;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the individualCaseSafetyDraft property.
     * 
     * @return
     *     possible object is
     *     {@link IndividualCaseSafetyDraftType }
     *     
     */
    public IndividualCaseSafetyDraftType getIndividualCaseSafetyDraft() {
        return individualCaseSafetyDraft;
    }

    /**
     * Sets the value of the individualCaseSafetyDraft property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndividualCaseSafetyDraftType }
     *     
     */
    public void setIndividualCaseSafetyDraft(IndividualCaseSafetyDraftType value) {
        this.individualCaseSafetyDraft = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
