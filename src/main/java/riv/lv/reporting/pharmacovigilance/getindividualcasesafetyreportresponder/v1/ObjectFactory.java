
package riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreportresponder.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreportresponder.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetIndividualCaseSafetyReport_QNAME = new QName("urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReportResponder:1", "GetIndividualCaseSafetyReport");
    private final static QName _GetIndividualCaseSafetyReportResponse_QNAME = new QName("urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReportResponder:1", "GetIndividualCaseSafetyReportResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreportresponder.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetIndividualCaseSafetyReportType }
     * 
     */
    public GetIndividualCaseSafetyReportType createGetIndividualCaseSafetyReportType() {
        return new GetIndividualCaseSafetyReportType();
    }

    /**
     * Create an instance of {@link GetIndividualCaseSafetyReportResponseType }
     * 
     */
    public GetIndividualCaseSafetyReportResponseType createGetIndividualCaseSafetyReportResponseType() {
        return new GetIndividualCaseSafetyReportResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIndividualCaseSafetyReportType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReportResponder:1", name = "GetIndividualCaseSafetyReport")
    public JAXBElement<GetIndividualCaseSafetyReportType> createGetIndividualCaseSafetyReport(GetIndividualCaseSafetyReportType value) {
        return new JAXBElement<GetIndividualCaseSafetyReportType>(_GetIndividualCaseSafetyReport_QNAME, GetIndividualCaseSafetyReportType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIndividualCaseSafetyReportResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReportResponder:1", name = "GetIndividualCaseSafetyReportResponse")
    public JAXBElement<GetIndividualCaseSafetyReportResponseType> createGetIndividualCaseSafetyReportResponse(GetIndividualCaseSafetyReportResponseType value) {
        return new JAXBElement<GetIndividualCaseSafetyReportResponseType>(_GetIndividualCaseSafetyReportResponse_QNAME, GetIndividualCaseSafetyReportResponseType.class, null, value);
    }

}
