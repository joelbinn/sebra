
package riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreportresponder.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;
import riv.lv.reporting.pharmacovigilance.v1.GetIndividualCaseSafetyReportType;


/**
 * <p>Java class for GetIndividualCaseSafetyReportResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIndividualCaseSafetyReportResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="individualCaseSafetyReport" type="{urn:riv:lv:reporting:pharmacovigilance:1}GetIndividualCaseSafetyReportType"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIndividualCaseSafetyReportResponseType", propOrder = {
    "individualCaseSafetyReport",
    "any"
})
public class GetIndividualCaseSafetyReportResponseType {

    @XmlElement(required = true)
    protected GetIndividualCaseSafetyReportType individualCaseSafetyReport;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the individualCaseSafetyReport property.
     * 
     * @return
     *     possible object is
     *     {@link GetIndividualCaseSafetyReportType }
     *     
     */
    public GetIndividualCaseSafetyReportType getIndividualCaseSafetyReport() {
        return individualCaseSafetyReport;
    }

    /**
     * Sets the value of the individualCaseSafetyReport property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetIndividualCaseSafetyReportType }
     *     
     */
    public void setIndividualCaseSafetyReport(GetIndividualCaseSafetyReportType value) {
        this.individualCaseSafetyReport = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
