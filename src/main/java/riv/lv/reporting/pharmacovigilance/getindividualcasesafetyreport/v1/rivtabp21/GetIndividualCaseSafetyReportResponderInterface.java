package riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreport.v1.rivtabp21;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.0.4
 * 2017-10-31T13:56:01.103+01:00
 * Generated source version: 3.0.4
 * 
 */
@WebService(targetNamespace = "urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReport:1:rivtabp21", name = "GetIndividualCaseSafetyReportResponderInterface")
@XmlSeeAlso({riv.lv.reporting.pharmacovigilance.enums.v1.ObjectFactory.class, riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreportresponder.v1.ObjectFactory.class, riv.lv.reporting.pharmacovigilance.v1.ObjectFactory.class, riv.itintegration.registry.v1.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface GetIndividualCaseSafetyReportResponderInterface {

    @WebMethod(operationName = "GetIndividualCaseSafetyReport", action = "urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReportResponder:1:GetIndividualCaseSafetyReport")
    @WebResult(name = "GetIndividualCaseSafetyReportResponse", targetNamespace = "urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReportResponder:1", partName = "parameters")
    public riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreportresponder.v1.GetIndividualCaseSafetyReportResponseType getIndividualCaseSafetyReport(
            @WebParam(partName = "LogicalAddress", name = "LogicalAddress", targetNamespace = "urn:riv:itintegration:registry:1", header = true)
                    String logicalAddress,
            @WebParam(partName = "parameters", name = "GetIndividualCaseSafetyReport", targetNamespace = "urn:riv:lv:reporting:pharmacovigilance:GetIndividualCaseSafetyReportResponder:1")
                    riv.lv.reporting.pharmacovigilance.getindividualcasesafetyreportresponder.v1.GetIndividualCaseSafetyReportType parameters
    );
}
