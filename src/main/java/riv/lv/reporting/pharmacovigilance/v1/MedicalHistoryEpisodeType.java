
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for MedicalHistoryEpisodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MedicalHistoryEpisodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="patientepisodename"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="250"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="patientmedicalstartdateformat" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfDateFormatEnum" minOccurs="0"/&gt;
 *         &lt;element name="patientmedicalstartdate" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedicalHistoryEpisodeType", propOrder = {
    "patientepisodename",
    "patientmedicalstartdateformat",
    "patientmedicalstartdate",
    "any"
})
public class MedicalHistoryEpisodeType {

    @XmlElement(required = true)
    protected String patientepisodename;
    protected String patientmedicalstartdateformat;
    protected String patientmedicalstartdate;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the patientepisodename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientepisodename() {
        return patientepisodename;
    }

    /**
     * Sets the value of the patientepisodename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientepisodename(String value) {
        this.patientepisodename = value;
    }

    /**
     * Gets the value of the patientmedicalstartdateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientmedicalstartdateformat() {
        return patientmedicalstartdateformat;
    }

    /**
     * Sets the value of the patientmedicalstartdateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientmedicalstartdateformat(String value) {
        this.patientmedicalstartdateformat = value;
    }

    /**
     * Gets the value of the patientmedicalstartdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientmedicalstartdate() {
        return patientmedicalstartdate;
    }

    /**
     * Sets the value of the patientmedicalstartdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientmedicalstartdate(String value) {
        this.patientmedicalstartdate = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
