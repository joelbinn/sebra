
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for SenderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SenderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sendertype" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="senderorganization" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="senderdepartment" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sendercountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" fixed="se" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SenderType", propOrder = {
    "sendertype",
    "senderorganization",
    "senderdepartment",
    "sendercountrycode",
    "any"
})
public class SenderType {

    @XmlElement(required = true)
    protected String sendertype;
    @XmlElement(required = true)
    protected String senderorganization;
    @XmlElement(required = true)
    protected String senderdepartment;
    @XmlElement(required = true)
    protected String sendercountrycode;
    @XmlAnyElement(lax = true)
    protected List<Object> any;
    @XmlAttribute(name = "lang")
    protected String lang;

    /**
     * Gets the value of the sendertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendertype() {
        return sendertype;
    }

    /**
     * Sets the value of the sendertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendertype(String value) {
        this.sendertype = value;
    }

    /**
     * Gets the value of the senderorganization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderorganization() {
        return senderorganization;
    }

    /**
     * Sets the value of the senderorganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderorganization(String value) {
        this.senderorganization = value;
    }

    /**
     * Gets the value of the senderdepartment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderdepartment() {
        return senderdepartment;
    }

    /**
     * Sets the value of the senderdepartment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderdepartment(String value) {
        this.senderdepartment = value;
    }

    /**
     * Gets the value of the sendercountrycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendercountrycode() {
        return sendercountrycode;
    }

    /**
     * Sets the value of the sendercountrycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendercountrycode(String value) {
        this.sendercountrycode = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

    /**
     * Gets the value of the lang property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLang() {
        if (lang == null) {
            return "se";
        } else {
            return lang;
        }
    }

    /**
     * Sets the value of the lang property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLang(String value) {
        this.lang = value;
    }

}
