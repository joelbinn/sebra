
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for SafetyReportType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SafetyReportType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="safetyreportversion"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="safetyreportid"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="primarysourcecountry" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="occurcountry" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="2"/&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="transmissiondateformat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="transmissiondate"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporttype" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serious" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfSeriousnessEnum"/&gt;
 *         &lt;element name="seriousnessdeath" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfSeriousnessEnum" minOccurs="0"/&gt;
 *         &lt;element name="seriousnesslifethreatening" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfSeriousnessEnum" minOccurs="0"/&gt;
 *         &lt;element name="seriousnesshospitalization" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfSeriousnessEnum" minOccurs="0"/&gt;
 *         &lt;element name="seriousnessdisabling" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfSeriousnessEnum" minOccurs="0"/&gt;
 *         &lt;element name="seriousnesscongenitalanomali" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfSeriousnessEnum" minOccurs="0"/&gt;
 *         &lt;element name="seriousnessother" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfSeriousnessEnum" minOccurs="0"/&gt;
 *         &lt;element name="receivedateformat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="receivedate"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="receiptdateformat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="receiptdate"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="companynumb"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="primarysource" type="{urn:riv:lv:reporting:pharmacovigilance:1}PrimarysourceType"/&gt;
 *         &lt;element name="sender" type="{urn:riv:lv:reporting:pharmacovigilance:1}SenderType"/&gt;
 *         &lt;element name="receiver" type="{urn:riv:lv:reporting:pharmacovigilance:1}ReceiverType"/&gt;
 *         &lt;element name="patient" type="{urn:riv:lv:reporting:pharmacovigilance:1}PatientType"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SafetyReportType", propOrder = {
    "safetyreportversion",
    "safetyreportid",
    "primarysourcecountry",
    "occurcountry",
    "transmissiondateformat",
    "transmissiondate",
    "reporttype",
    "serious",
    "seriousnessdeath",
    "seriousnesslifethreatening",
    "seriousnesshospitalization",
    "seriousnessdisabling",
    "seriousnesscongenitalanomali",
    "seriousnessother",
    "receivedateformat",
    "receivedate",
    "receiptdateformat",
    "receiptdate",
    "companynumb",
    "primarysource",
    "sender",
    "receiver",
    "patient",
    "any"
})
public class SafetyReportType {

    @XmlElement(required = true)
    protected String safetyreportversion;
    @XmlElement(required = true)
    protected String safetyreportid;
    @XmlElement(required = true)
    protected String primarysourcecountry;
    protected String occurcountry;
    @XmlElement(required = true)
    protected String transmissiondateformat;
    @XmlElement(required = true)
    protected String transmissiondate;
    @XmlElement(required = true)
    protected String reporttype;
    @XmlElement(required = true)
    protected String serious;
    protected String seriousnessdeath;
    protected String seriousnesslifethreatening;
    protected String seriousnesshospitalization;
    protected String seriousnessdisabling;
    protected String seriousnesscongenitalanomali;
    protected String seriousnessother;
    @XmlElement(required = true)
    protected String receivedateformat;
    @XmlElement(required = true)
    protected String receivedate;
    @XmlElement(required = true)
    protected String receiptdateformat;
    @XmlElement(required = true)
    protected String receiptdate;
    @XmlElement(required = true)
    protected String companynumb;
    @XmlElement(required = true)
    protected PrimarysourceType primarysource;
    @XmlElement(required = true)
    protected SenderType sender;
    @XmlElement(required = true)
    protected ReceiverType receiver;
    @XmlElement(required = true)
    protected PatientType patient;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the safetyreportversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSafetyreportversion() {
        return safetyreportversion;
    }

    /**
     * Sets the value of the safetyreportversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSafetyreportversion(String value) {
        this.safetyreportversion = value;
    }

    /**
     * Gets the value of the safetyreportid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSafetyreportid() {
        return safetyreportid;
    }

    /**
     * Sets the value of the safetyreportid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSafetyreportid(String value) {
        this.safetyreportid = value;
    }

    /**
     * Gets the value of the primarysourcecountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimarysourcecountry() {
        return primarysourcecountry;
    }

    /**
     * Sets the value of the primarysourcecountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimarysourcecountry(String value) {
        this.primarysourcecountry = value;
    }

    /**
     * Gets the value of the occurcountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccurcountry() {
        return occurcountry;
    }

    /**
     * Sets the value of the occurcountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccurcountry(String value) {
        this.occurcountry = value;
    }

    /**
     * Gets the value of the transmissiondateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissiondateformat() {
        return transmissiondateformat;
    }

    /**
     * Sets the value of the transmissiondateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissiondateformat(String value) {
        this.transmissiondateformat = value;
    }

    /**
     * Gets the value of the transmissiondate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissiondate() {
        return transmissiondate;
    }

    /**
     * Sets the value of the transmissiondate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissiondate(String value) {
        this.transmissiondate = value;
    }

    /**
     * Gets the value of the reporttype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporttype() {
        return reporttype;
    }

    /**
     * Sets the value of the reporttype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporttype(String value) {
        this.reporttype = value;
    }

    /**
     * Gets the value of the serious property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerious() {
        return serious;
    }

    /**
     * Sets the value of the serious property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerious(String value) {
        this.serious = value;
    }

    /**
     * Gets the value of the seriousnessdeath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeriousnessdeath() {
        return seriousnessdeath;
    }

    /**
     * Sets the value of the seriousnessdeath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeriousnessdeath(String value) {
        this.seriousnessdeath = value;
    }

    /**
     * Gets the value of the seriousnesslifethreatening property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeriousnesslifethreatening() {
        return seriousnesslifethreatening;
    }

    /**
     * Sets the value of the seriousnesslifethreatening property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeriousnesslifethreatening(String value) {
        this.seriousnesslifethreatening = value;
    }

    /**
     * Gets the value of the seriousnesshospitalization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeriousnesshospitalization() {
        return seriousnesshospitalization;
    }

    /**
     * Sets the value of the seriousnesshospitalization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeriousnesshospitalization(String value) {
        this.seriousnesshospitalization = value;
    }

    /**
     * Gets the value of the seriousnessdisabling property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeriousnessdisabling() {
        return seriousnessdisabling;
    }

    /**
     * Sets the value of the seriousnessdisabling property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeriousnessdisabling(String value) {
        this.seriousnessdisabling = value;
    }

    /**
     * Gets the value of the seriousnesscongenitalanomali property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeriousnesscongenitalanomali() {
        return seriousnesscongenitalanomali;
    }

    /**
     * Sets the value of the seriousnesscongenitalanomali property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeriousnesscongenitalanomali(String value) {
        this.seriousnesscongenitalanomali = value;
    }

    /**
     * Gets the value of the seriousnessother property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeriousnessother() {
        return seriousnessother;
    }

    /**
     * Sets the value of the seriousnessother property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeriousnessother(String value) {
        this.seriousnessother = value;
    }

    /**
     * Gets the value of the receivedateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedateformat() {
        return receivedateformat;
    }

    /**
     * Sets the value of the receivedateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedateformat(String value) {
        this.receivedateformat = value;
    }

    /**
     * Gets the value of the receivedate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedate() {
        return receivedate;
    }

    /**
     * Sets the value of the receivedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedate(String value) {
        this.receivedate = value;
    }

    /**
     * Gets the value of the receiptdateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptdateformat() {
        return receiptdateformat;
    }

    /**
     * Sets the value of the receiptdateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptdateformat(String value) {
        this.receiptdateformat = value;
    }

    /**
     * Gets the value of the receiptdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptdate() {
        return receiptdate;
    }

    /**
     * Sets the value of the receiptdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptdate(String value) {
        this.receiptdate = value;
    }

    /**
     * Gets the value of the companynumb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanynumb() {
        return companynumb;
    }

    /**
     * Sets the value of the companynumb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanynumb(String value) {
        this.companynumb = value;
    }

    /**
     * Gets the value of the primarysource property.
     * 
     * @return
     *     possible object is
     *     {@link PrimarysourceType }
     *     
     */
    public PrimarysourceType getPrimarysource() {
        return primarysource;
    }

    /**
     * Sets the value of the primarysource property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimarysourceType }
     *     
     */
    public void setPrimarysource(PrimarysourceType value) {
        this.primarysource = value;
    }

    /**
     * Gets the value of the sender property.
     * 
     * @return
     *     possible object is
     *     {@link SenderType }
     *     
     */
    public SenderType getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param value
     *     allowed object is
     *     {@link SenderType }
     *     
     */
    public void setSender(SenderType value) {
        this.sender = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiverType }
     *     
     */
    public ReceiverType getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiverType }
     *     
     */
    public void setReceiver(ReceiverType value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the patient property.
     * 
     * @return
     *     possible object is
     *     {@link PatientType }
     *     
     */
    public PatientType getPatient() {
        return patient;
    }

    /**
     * Sets the value of the patient property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientType }
     *     
     */
    public void setPatient(PatientType value) {
        this.patient = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
