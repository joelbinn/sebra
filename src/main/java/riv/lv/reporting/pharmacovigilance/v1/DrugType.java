
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for DrugType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DrugType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="drugcharacterization" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfDrugCharacterizationEnum"/&gt;
 *         &lt;element name="medicinalproduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="drugbatchnumb" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="drugauthorizationnumb" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="drugauthorizationholder" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="60"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="drugdosagetext" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="drugdosageform" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="drugadministrationroute" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="0{2}[1-9]|0[1-5][0-9]|06[0-7]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="drugstartdateformat" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfDateFormatEnum" minOccurs="0"/&gt;
 *         &lt;element name="drugstartdate" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="drugenddateformat" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfDateFormatEnum" minOccurs="0"/&gt;
 *         &lt;element name="drugenddate" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="activesubstance" type="{urn:riv:lv:reporting:pharmacovigilance:1}ActiveSubstanceType" minOccurs="0"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DrugType", propOrder = {
    "drugcharacterization",
    "medicinalproduct",
    "drugbatchnumb",
    "drugauthorizationnumb",
    "drugauthorizationholder",
    "drugdosagetext",
    "drugdosageform",
    "drugadministrationroute",
    "drugstartdateformat",
    "drugstartdate",
    "drugenddateformat",
    "drugenddate",
    "activesubstance",
    "any"
})
public class DrugType {

    @XmlElement(required = true)
    protected String drugcharacterization;
    @XmlElement(required = true)
    protected String medicinalproduct;
    protected String drugbatchnumb;
    protected String drugauthorizationnumb;
    protected String drugauthorizationholder;
    protected String drugdosagetext;
    protected String drugdosageform;
    protected String drugadministrationroute;
    protected String drugstartdateformat;
    protected String drugstartdate;
    protected String drugenddateformat;
    protected String drugenddate;
    protected ActiveSubstanceType activesubstance;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the drugcharacterization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugcharacterization() {
        return drugcharacterization;
    }

    /**
     * Sets the value of the drugcharacterization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugcharacterization(String value) {
        this.drugcharacterization = value;
    }

    /**
     * Gets the value of the medicinalproduct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicinalproduct() {
        return medicinalproduct;
    }

    /**
     * Sets the value of the medicinalproduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicinalproduct(String value) {
        this.medicinalproduct = value;
    }

    /**
     * Gets the value of the drugbatchnumb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugbatchnumb() {
        return drugbatchnumb;
    }

    /**
     * Sets the value of the drugbatchnumb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugbatchnumb(String value) {
        this.drugbatchnumb = value;
    }

    /**
     * Gets the value of the drugauthorizationnumb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugauthorizationnumb() {
        return drugauthorizationnumb;
    }

    /**
     * Sets the value of the drugauthorizationnumb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugauthorizationnumb(String value) {
        this.drugauthorizationnumb = value;
    }

    /**
     * Gets the value of the drugauthorizationholder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugauthorizationholder() {
        return drugauthorizationholder;
    }

    /**
     * Sets the value of the drugauthorizationholder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugauthorizationholder(String value) {
        this.drugauthorizationholder = value;
    }

    /**
     * Gets the value of the drugdosagetext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugdosagetext() {
        return drugdosagetext;
    }

    /**
     * Sets the value of the drugdosagetext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugdosagetext(String value) {
        this.drugdosagetext = value;
    }

    /**
     * Gets the value of the drugdosageform property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugdosageform() {
        return drugdosageform;
    }

    /**
     * Sets the value of the drugdosageform property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugdosageform(String value) {
        this.drugdosageform = value;
    }

    /**
     * Gets the value of the drugadministrationroute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugadministrationroute() {
        return drugadministrationroute;
    }

    /**
     * Sets the value of the drugadministrationroute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugadministrationroute(String value) {
        this.drugadministrationroute = value;
    }

    /**
     * Gets the value of the drugstartdateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugstartdateformat() {
        return drugstartdateformat;
    }

    /**
     * Sets the value of the drugstartdateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugstartdateformat(String value) {
        this.drugstartdateformat = value;
    }

    /**
     * Gets the value of the drugstartdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugstartdate() {
        return drugstartdate;
    }

    /**
     * Sets the value of the drugstartdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugstartdate(String value) {
        this.drugstartdate = value;
    }

    /**
     * Gets the value of the drugenddateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugenddateformat() {
        return drugenddateformat;
    }

    /**
     * Sets the value of the drugenddateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugenddateformat(String value) {
        this.drugenddateformat = value;
    }

    /**
     * Gets the value of the drugenddate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrugenddate() {
        return drugenddate;
    }

    /**
     * Sets the value of the drugenddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrugenddate(String value) {
        this.drugenddate = value;
    }

    /**
     * Gets the value of the activesubstance property.
     * 
     * @return
     *     possible object is
     *     {@link ActiveSubstanceType }
     *     
     */
    public ActiveSubstanceType getActivesubstance() {
        return activesubstance;
    }

    /**
     * Sets the value of the activesubstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActiveSubstanceType }
     *     
     */
    public void setActivesubstance(ActiveSubstanceType value) {
        this.activesubstance = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
