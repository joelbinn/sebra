
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for PatientType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PatientType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="patientbirthdateformat" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfDateFormatEnum"/&gt;
 *         &lt;element name="patientbirthdate"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="patientonsetage"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="patientonsetageunit" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfPatientOnsetAgeUnitEnum"/&gt;
 *         &lt;element name="gestationperiod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="gestationperiodunit" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfGestationPeriodUnitEnum" minOccurs="0"/&gt;
 *         &lt;element name="patientsex" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfPatientSexEnum" minOccurs="0"/&gt;
 *         &lt;element name="patientmedicalhistorytext" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10000"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="medicalhistoryepisode" type="{urn:riv:lv:reporting:pharmacovigilance:1}MedicalHistoryEpisodeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="patientdeath" type="{urn:riv:lv:reporting:pharmacovigilance:1}PatientDeathType" minOccurs="0"/&gt;
 *         &lt;element name="parent" type="{urn:riv:lv:reporting:pharmacovigilance:1}ParentType"/&gt;
 *         &lt;element name="reaction" type="{urn:riv:lv:reporting:pharmacovigilance:1}ReactionType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="drug" type="{urn:riv:lv:reporting:pharmacovigilance:1}DrugType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="summary" type="{urn:riv:lv:reporting:pharmacovigilance:1}SummaryType" minOccurs="0"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientType", propOrder = {
    "patientbirthdateformat",
    "patientbirthdate",
    "patientonsetage",
    "patientonsetageunit",
    "gestationperiod",
    "gestationperiodunit",
    "patientsex",
    "patientmedicalhistorytext",
    "medicalhistoryepisode",
    "patientdeath",
    "parent",
    "reaction",
    "drug",
    "summary",
    "any"
})
public class PatientType {

    @XmlElement(required = true)
    protected String patientbirthdateformat;
    @XmlElement(required = true)
    protected String patientbirthdate;
    @XmlElement(required = true)
    protected String patientonsetage;
    @XmlElement(required = true)
    protected String patientonsetageunit;
    protected String gestationperiod;
    protected String gestationperiodunit;
    protected String patientsex;
    protected String patientmedicalhistorytext;
    protected List<MedicalHistoryEpisodeType> medicalhistoryepisode;
    protected PatientDeathType patientdeath;
    @XmlElement(required = true)
    protected ParentType parent;
    @XmlElement(required = true)
    protected List<ReactionType> reaction;
    @XmlElement(required = true)
    protected List<DrugType> drug;
    protected SummaryType summary;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the patientbirthdateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientbirthdateformat() {
        return patientbirthdateformat;
    }

    /**
     * Sets the value of the patientbirthdateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientbirthdateformat(String value) {
        this.patientbirthdateformat = value;
    }

    /**
     * Gets the value of the patientbirthdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientbirthdate() {
        return patientbirthdate;
    }

    /**
     * Sets the value of the patientbirthdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientbirthdate(String value) {
        this.patientbirthdate = value;
    }

    /**
     * Gets the value of the patientonsetage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientonsetage() {
        return patientonsetage;
    }

    /**
     * Sets the value of the patientonsetage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientonsetage(String value) {
        this.patientonsetage = value;
    }

    /**
     * Gets the value of the patientonsetageunit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientonsetageunit() {
        return patientonsetageunit;
    }

    /**
     * Sets the value of the patientonsetageunit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientonsetageunit(String value) {
        this.patientonsetageunit = value;
    }

    /**
     * Gets the value of the gestationperiod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGestationperiod() {
        return gestationperiod;
    }

    /**
     * Sets the value of the gestationperiod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGestationperiod(String value) {
        this.gestationperiod = value;
    }

    /**
     * Gets the value of the gestationperiodunit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGestationperiodunit() {
        return gestationperiodunit;
    }

    /**
     * Sets the value of the gestationperiodunit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGestationperiodunit(String value) {
        this.gestationperiodunit = value;
    }

    /**
     * Gets the value of the patientsex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientsex() {
        return patientsex;
    }

    /**
     * Sets the value of the patientsex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientsex(String value) {
        this.patientsex = value;
    }

    /**
     * Gets the value of the patientmedicalhistorytext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientmedicalhistorytext() {
        return patientmedicalhistorytext;
    }

    /**
     * Sets the value of the patientmedicalhistorytext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientmedicalhistorytext(String value) {
        this.patientmedicalhistorytext = value;
    }

    /**
     * Gets the value of the medicalhistoryepisode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the medicalhistoryepisode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMedicalhistoryepisode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MedicalHistoryEpisodeType }
     * 
     * 
     */
    public List<MedicalHistoryEpisodeType> getMedicalhistoryepisode() {
        if (medicalhistoryepisode == null) {
            medicalhistoryepisode = new ArrayList<MedicalHistoryEpisodeType>();
        }
        return this.medicalhistoryepisode;
    }

    /**
     * Gets the value of the patientdeath property.
     * 
     * @return
     *     possible object is
     *     {@link PatientDeathType }
     *     
     */
    public PatientDeathType getPatientdeath() {
        return patientdeath;
    }

    /**
     * Sets the value of the patientdeath property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientDeathType }
     *     
     */
    public void setPatientdeath(PatientDeathType value) {
        this.patientdeath = value;
    }

    /**
     * Gets the value of the parent property.
     * 
     * @return
     *     possible object is
     *     {@link ParentType }
     *     
     */
    public ParentType getParent() {
        return parent;
    }

    /**
     * Sets the value of the parent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentType }
     *     
     */
    public void setParent(ParentType value) {
        this.parent = value;
    }

    /**
     * Gets the value of the reaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReactionType }
     * 
     * 
     */
    public List<ReactionType> getReaction() {
        if (reaction == null) {
            reaction = new ArrayList<ReactionType>();
        }
        return this.reaction;
    }

    /**
     * Gets the value of the drug property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the drug property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDrug().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DrugType }
     * 
     * 
     */
    public List<DrugType> getDrug() {
        if (drug == null) {
            drug = new ArrayList<DrugType>();
        }
        return this.drug;
    }

    /**
     * Gets the value of the summary property.
     * 
     * @return
     *     possible object is
     *     {@link SummaryType }
     *     
     */
    public SummaryType getSummary() {
        return summary;
    }

    /**
     * Sets the value of the summary property.
     * 
     * @param value
     *     allowed object is
     *     {@link SummaryType }
     *     
     */
    public void setSummary(SummaryType value) {
        this.summary = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
