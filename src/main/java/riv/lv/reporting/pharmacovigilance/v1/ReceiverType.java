
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;
import riv.lv.reporting.pharmacovigilance.enums.v1.TypeOfMessageReceiverIdentifierEnum;


/**
 * <p>Java class for ReceiverType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReceiverType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="receivertype" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="receiverorganization" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfMessageReceiverIdentifierEnum"/&gt;
 *         &lt;element name="receiverdepartment" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="receivercountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiverType", propOrder = {
    "receivertype",
    "receiverorganization",
    "receiverdepartment",
    "receivercountrycode",
    "any"
})
public class ReceiverType {

    @XmlElement(required = true)
    protected String receivertype;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TypeOfMessageReceiverIdentifierEnum receiverorganization;
    @XmlElement(required = true)
    protected String receiverdepartment;
    @XmlElement(required = true)
    protected String receivercountrycode;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the receivertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivertype() {
        return receivertype;
    }

    /**
     * Sets the value of the receivertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivertype(String value) {
        this.receivertype = value;
    }

    /**
     * Gets the value of the receiverorganization property.
     * 
     * @return
     *     possible object is
     *     {@link TypeOfMessageReceiverIdentifierEnum }
     *     
     */
    public TypeOfMessageReceiverIdentifierEnum getReceiverorganization() {
        return receiverorganization;
    }

    /**
     * Sets the value of the receiverorganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeOfMessageReceiverIdentifierEnum }
     *     
     */
    public void setReceiverorganization(TypeOfMessageReceiverIdentifierEnum value) {
        this.receiverorganization = value;
    }

    /**
     * Gets the value of the receiverdepartment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiverdepartment() {
        return receiverdepartment;
    }

    /**
     * Sets the value of the receiverdepartment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiverdepartment(String value) {
        this.receiverdepartment = value;
    }

    /**
     * Gets the value of the receivercountrycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivercountrycode() {
        return receivercountrycode;
    }

    /**
     * Sets the value of the receivercountrycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivercountrycode(String value) {
        this.receivercountrycode = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
