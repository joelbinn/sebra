
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for ReactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReactionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="primarysourcereaction"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="200"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="termhighlighted" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfTermHighlightedEnum"/&gt;
 *         &lt;element name="reactionstartdateformat" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfReactionStartDateEnum"/&gt;
 *         &lt;element name="reactionstartdate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="reactionoutcome" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOReactionOutcomeEnum"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReactionType", propOrder = {
    "primarysourcereaction",
    "termhighlighted",
    "reactionstartdateformat",
    "reactionstartdate",
    "reactionoutcome",
    "any"
})
public class ReactionType {

    @XmlElement(required = true)
    protected String primarysourcereaction;
    @XmlElement(required = true)
    protected String termhighlighted;
    @XmlElement(required = true)
    protected String reactionstartdateformat;
    @XmlElement(required = true)
    protected String reactionstartdate;
    @XmlElement(required = true)
    protected String reactionoutcome;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the primarysourcereaction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimarysourcereaction() {
        return primarysourcereaction;
    }

    /**
     * Sets the value of the primarysourcereaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimarysourcereaction(String value) {
        this.primarysourcereaction = value;
    }

    /**
     * Gets the value of the termhighlighted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermhighlighted() {
        return termhighlighted;
    }

    /**
     * Sets the value of the termhighlighted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermhighlighted(String value) {
        this.termhighlighted = value;
    }

    /**
     * Gets the value of the reactionstartdateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReactionstartdateformat() {
        return reactionstartdateformat;
    }

    /**
     * Sets the value of the reactionstartdateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReactionstartdateformat(String value) {
        this.reactionstartdateformat = value;
    }

    /**
     * Gets the value of the reactionstartdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReactionstartdate() {
        return reactionstartdate;
    }

    /**
     * Sets the value of the reactionstartdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReactionstartdate(String value) {
        this.reactionstartdate = value;
    }

    /**
     * Gets the value of the reactionoutcome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReactionoutcome() {
        return reactionoutcome;
    }

    /**
     * Sets the value of the reactionoutcome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReactionoutcome(String value) {
        this.reactionoutcome = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
