
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for IndividualCaseSafetyDraftType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndividualCaseSafetyDraftType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="patientId" type="{urn:riv:lv:reporting:pharmacovigilance:1}IIType"/&gt;
 *         &lt;element name="healthcareProfessionalId" type="{urn:riv:lv:reporting:pharmacovigilance:1}IIType"/&gt;
 *         &lt;element name="caregiverId" type="{urn:riv:lv:reporting:pharmacovigilance:1}IIType"/&gt;
 *         &lt;element name="careUnitId" type="{urn:riv:lv:reporting:pharmacovigilance:1}IIType"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndividualCaseSafetyDraftType", propOrder = {
    "patientId",
    "healthcareProfessionalId",
    "caregiverId",
    "careUnitId",
    "any"
})
public class IndividualCaseSafetyDraftType {

    @XmlElement(required = true)
    protected IIType patientId;
    @XmlElement(required = true)
    protected IIType healthcareProfessionalId;
    @XmlElement(required = true)
    protected IIType caregiverId;
    @XmlElement(required = true)
    protected IIType careUnitId;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the patientId property.
     * 
     * @return
     *     possible object is
     *     {@link IIType }
     *     
     */
    public IIType getPatientId() {
        return patientId;
    }

    /**
     * Sets the value of the patientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IIType }
     *     
     */
    public void setPatientId(IIType value) {
        this.patientId = value;
    }

    /**
     * Gets the value of the healthcareProfessionalId property.
     * 
     * @return
     *     possible object is
     *     {@link IIType }
     *     
     */
    public IIType getHealthcareProfessionalId() {
        return healthcareProfessionalId;
    }

    /**
     * Sets the value of the healthcareProfessionalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IIType }
     *     
     */
    public void setHealthcareProfessionalId(IIType value) {
        this.healthcareProfessionalId = value;
    }

    /**
     * Gets the value of the caregiverId property.
     * 
     * @return
     *     possible object is
     *     {@link IIType }
     *     
     */
    public IIType getCaregiverId() {
        return caregiverId;
    }

    /**
     * Sets the value of the caregiverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IIType }
     *     
     */
    public void setCaregiverId(IIType value) {
        this.caregiverId = value;
    }

    /**
     * Gets the value of the careUnitId property.
     * 
     * @return
     *     possible object is
     *     {@link IIType }
     *     
     */
    public IIType getCareUnitId() {
        return careUnitId;
    }

    /**
     * Sets the value of the careUnitId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IIType }
     *     
     */
    public void setCareUnitId(IIType value) {
        this.careUnitId = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
