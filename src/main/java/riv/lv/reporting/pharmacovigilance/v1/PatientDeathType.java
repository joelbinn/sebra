
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for PatientDeathType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PatientDeathType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="patientdeathdateformat" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfDateFormatEnum"/&gt;
 *         &lt;element name="patientdeathdate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="patientdeathcause" type="{urn:riv:lv:reporting:pharmacovigilance:1}PatientDeathCauseType" minOccurs="0"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientDeathType", propOrder = {
    "patientdeathdateformat",
    "patientdeathdate",
    "patientdeathcause",
    "any"
})
public class PatientDeathType {

    @XmlElement(required = true)
    protected String patientdeathdateformat;
    @XmlElement(required = true)
    protected String patientdeathdate;
    protected PatientDeathCauseType patientdeathcause;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the patientdeathdateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientdeathdateformat() {
        return patientdeathdateformat;
    }

    /**
     * Sets the value of the patientdeathdateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientdeathdateformat(String value) {
        this.patientdeathdateformat = value;
    }

    /**
     * Gets the value of the patientdeathdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientdeathdate() {
        return patientdeathdate;
    }

    /**
     * Sets the value of the patientdeathdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientdeathdate(String value) {
        this.patientdeathdate = value;
    }

    /**
     * Gets the value of the patientdeathcause property.
     * 
     * @return
     *     possible object is
     *     {@link PatientDeathCauseType }
     *     
     */
    public PatientDeathCauseType getPatientdeathcause() {
        return patientdeathcause;
    }

    /**
     * Sets the value of the patientdeathcause property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientDeathCauseType }
     *     
     */
    public void setPatientdeathcause(PatientDeathCauseType value) {
        this.patientdeathcause = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
