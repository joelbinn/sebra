
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;
import riv.lv.reporting.pharmacovigilance.enums.v1.TypeOfMessageReceiverIdentifierEnum;


/**
 * <p>Java class for ICHICSRMessageHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICHICSRMessageHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="messagetype" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="messageformatversion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="messageformatrelease" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="messagenumb"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="messagesenderidentifier" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="messagereceiveridentifier" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfMessageReceiverIdentifierEnum"/&gt;
 *         &lt;element name="messagedateformat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="messagedate"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="14"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICHICSRMessageHeaderType", propOrder = {
    "messagetype",
    "messageformatversion",
    "messageformatrelease",
    "messagenumb",
    "messagesenderidentifier",
    "messagereceiveridentifier",
    "messagedateformat",
    "messagedate",
    "any"
})
public class ICHICSRMessageHeaderType {

    @XmlElement(required = true)
    protected String messagetype;
    @XmlElement(required = true)
    protected String messageformatversion;
    @XmlElement(required = true)
    protected String messageformatrelease;
    @XmlElement(required = true)
    protected String messagenumb;
    @XmlElement(required = true)
    protected String messagesenderidentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TypeOfMessageReceiverIdentifierEnum messagereceiveridentifier;
    @XmlElement(required = true)
    protected String messagedateformat;
    @XmlElement(required = true)
    protected String messagedate;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the messagetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagetype() {
        return messagetype;
    }

    /**
     * Sets the value of the messagetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagetype(String value) {
        this.messagetype = value;
    }

    /**
     * Gets the value of the messageformatversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageformatversion() {
        return messageformatversion;
    }

    /**
     * Sets the value of the messageformatversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageformatversion(String value) {
        this.messageformatversion = value;
    }

    /**
     * Gets the value of the messageformatrelease property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageformatrelease() {
        return messageformatrelease;
    }

    /**
     * Sets the value of the messageformatrelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageformatrelease(String value) {
        this.messageformatrelease = value;
    }

    /**
     * Gets the value of the messagenumb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagenumb() {
        return messagenumb;
    }

    /**
     * Sets the value of the messagenumb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagenumb(String value) {
        this.messagenumb = value;
    }

    /**
     * Gets the value of the messagesenderidentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagesenderidentifier() {
        return messagesenderidentifier;
    }

    /**
     * Sets the value of the messagesenderidentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagesenderidentifier(String value) {
        this.messagesenderidentifier = value;
    }

    /**
     * Gets the value of the messagereceiveridentifier property.
     * 
     * @return
     *     possible object is
     *     {@link TypeOfMessageReceiverIdentifierEnum }
     *     
     */
    public TypeOfMessageReceiverIdentifierEnum getMessagereceiveridentifier() {
        return messagereceiveridentifier;
    }

    /**
     * Sets the value of the messagereceiveridentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeOfMessageReceiverIdentifierEnum }
     *     
     */
    public void setMessagereceiveridentifier(TypeOfMessageReceiverIdentifierEnum value) {
        this.messagereceiveridentifier = value;
    }

    /**
     * Gets the value of the messagedateformat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagedateformat() {
        return messagedateformat;
    }

    /**
     * Sets the value of the messagedateformat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagedateformat(String value) {
        this.messagedateformat = value;
    }

    /**
     * Gets the value of the messagedate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagedate() {
        return messagedate;
    }

    /**
     * Sets the value of the messagedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagedate(String value) {
        this.messagedate = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
