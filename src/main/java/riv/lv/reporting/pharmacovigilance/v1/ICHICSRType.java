
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for ICHICSRType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ICHICSRType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ichicsrmessageheader" type="{urn:riv:lv:reporting:pharmacovigilance:1}ICHICSRMessageHeaderType"/&gt;
 *         &lt;element name="safetyreport" type="{urn:riv:lv:reporting:pharmacovigilance:1}SafetyReportType"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" fixed="se" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ICHICSRType", propOrder = {
    "ichicsrmessageheader",
    "safetyreport",
    "any"
})
public class ICHICSRType {

    @XmlElement(required = true)
    protected ICHICSRMessageHeaderType ichicsrmessageheader;
    @XmlElement(required = true)
    protected SafetyReportType safetyreport;
    @XmlAnyElement(lax = true)
    protected List<Object> any;
    @XmlAttribute(name = "lang")
    protected String lang;

    /**
     * Gets the value of the ichicsrmessageheader property.
     * 
     * @return
     *     possible object is
     *     {@link ICHICSRMessageHeaderType }
     *     
     */
    public ICHICSRMessageHeaderType getIchicsrmessageheader() {
        return ichicsrmessageheader;
    }

    /**
     * Sets the value of the ichicsrmessageheader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ICHICSRMessageHeaderType }
     *     
     */
    public void setIchicsrmessageheader(ICHICSRMessageHeaderType value) {
        this.ichicsrmessageheader = value;
    }

    /**
     * Gets the value of the safetyreport property.
     * 
     * @return
     *     possible object is
     *     {@link SafetyReportType }
     *     
     */
    public SafetyReportType getSafetyreport() {
        return safetyreport;
    }

    /**
     * Sets the value of the safetyreport property.
     * 
     * @param value
     *     allowed object is
     *     {@link SafetyReportType }
     *     
     */
    public void setSafetyreport(SafetyReportType value) {
        this.safetyreport = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

    /**
     * Gets the value of the lang property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLang() {
        if (lang == null) {
            return "se";
        } else {
            return lang;
        }
    }

    /**
     * Sets the value of the lang property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLang(String value) {
        this.lang = value;
    }

}
