
package riv.lv.reporting.pharmacovigilance.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the riv.lv.reporting.pharmacovigilance.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: riv.lv.reporting.pharmacovigilance.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IndividualCaseSafetyDraftType }
     * 
     */
    public IndividualCaseSafetyDraftType createIndividualCaseSafetyDraftType() {
        return new IndividualCaseSafetyDraftType();
    }

    /**
     * Create an instance of {@link IndividualCaseSafetyReportType }
     * 
     */
    public IndividualCaseSafetyReportType createIndividualCaseSafetyReportType() {
        return new IndividualCaseSafetyReportType();
    }

    /**
     * Create an instance of {@link ICHICSRType }
     * 
     */
    public ICHICSRType createICHICSRType() {
        return new ICHICSRType();
    }

    /**
     * Create an instance of {@link ICHICSRMessageHeaderType }
     * 
     */
    public ICHICSRMessageHeaderType createICHICSRMessageHeaderType() {
        return new ICHICSRMessageHeaderType();
    }

    /**
     * Create an instance of {@link SafetyReportType }
     * 
     */
    public SafetyReportType createSafetyReportType() {
        return new SafetyReportType();
    }

    /**
     * Create an instance of {@link PrimarysourceType }
     * 
     */
    public PrimarysourceType createPrimarysourceType() {
        return new PrimarysourceType();
    }

    /**
     * Create an instance of {@link SenderType }
     * 
     */
    public SenderType createSenderType() {
        return new SenderType();
    }

    /**
     * Create an instance of {@link ParentType }
     * 
     */
    public ParentType createParentType() {
        return new ParentType();
    }

    /**
     * Create an instance of {@link ReceiverType }
     * 
     */
    public ReceiverType createReceiverType() {
        return new ReceiverType();
    }

    /**
     * Create an instance of {@link PatientType }
     * 
     */
    public PatientType createPatientType() {
        return new PatientType();
    }

    /**
     * Create an instance of {@link MedicalHistoryEpisodeType }
     * 
     */
    public MedicalHistoryEpisodeType createMedicalHistoryEpisodeType() {
        return new MedicalHistoryEpisodeType();
    }

    /**
     * Create an instance of {@link PatientDeathType }
     * 
     */
    public PatientDeathType createPatientDeathType() {
        return new PatientDeathType();
    }

    /**
     * Create an instance of {@link PatientDeathCauseType }
     * 
     */
    public PatientDeathCauseType createPatientDeathCauseType() {
        return new PatientDeathCauseType();
    }

    /**
     * Create an instance of {@link ReactionType }
     * 
     */
    public ReactionType createReactionType() {
        return new ReactionType();
    }

    /**
     * Create an instance of {@link DrugType }
     * 
     */
    public DrugType createDrugType() {
        return new DrugType();
    }

    /**
     * Create an instance of {@link ActiveSubstanceType }
     * 
     */
    public ActiveSubstanceType createActiveSubstanceType() {
        return new ActiveSubstanceType();
    }

    /**
     * Create an instance of {@link SummaryType }
     * 
     */
    public SummaryType createSummaryType() {
        return new SummaryType();
    }

    /**
     * Create an instance of {@link GetIndividualCaseSafetyReportType }
     * 
     */
    public GetIndividualCaseSafetyReportType createGetIndividualCaseSafetyReportType() {
        return new GetIndividualCaseSafetyReportType();
    }

    /**
     * Create an instance of {@link MultimediaType }
     * 
     */
    public MultimediaType createMultimediaType() {
        return new MultimediaType();
    }

    /**
     * Create an instance of {@link IIType }
     * 
     */
    public IIType createIIType() {
        return new IIType();
    }

}
