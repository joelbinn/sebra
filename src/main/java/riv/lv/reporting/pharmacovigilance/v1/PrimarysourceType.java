
package riv.lv.reporting.pharmacovigilance.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for PrimarysourceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PrimarysourceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reportertitle" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reportergivename" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporterfamilyname" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporterorganization" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="60"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporterdepartment" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="60"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporterstreet" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reportercity" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporterstate" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="40"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporterpostcode" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="15"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reportercountry" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="qualification" type="{urn:riv:lv:reporting:pharmacovigilance:enums:1}TypeOfQualificationEnum"/&gt;
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimarysourceType", propOrder = {
    "reportertitle",
    "reportergivename",
    "reporterfamilyname",
    "reporterorganization",
    "reporterdepartment",
    "reporterstreet",
    "reportercity",
    "reporterstate",
    "reporterpostcode",
    "reportercountry",
    "qualification",
    "any"
})
public class PrimarysourceType {

    protected String reportertitle;
    protected String reportergivename;
    protected String reporterfamilyname;
    protected String reporterorganization;
    protected String reporterdepartment;
    protected String reporterstreet;
    protected String reportercity;
    protected String reporterstate;
    protected String reporterpostcode;
    protected String reportercountry;
    @XmlElement(required = true)
    protected String qualification;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the reportertitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportertitle() {
        return reportertitle;
    }

    /**
     * Sets the value of the reportertitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportertitle(String value) {
        this.reportertitle = value;
    }

    /**
     * Gets the value of the reportergivename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportergivename() {
        return reportergivename;
    }

    /**
     * Sets the value of the reportergivename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportergivename(String value) {
        this.reportergivename = value;
    }

    /**
     * Gets the value of the reporterfamilyname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterfamilyname() {
        return reporterfamilyname;
    }

    /**
     * Sets the value of the reporterfamilyname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterfamilyname(String value) {
        this.reporterfamilyname = value;
    }

    /**
     * Gets the value of the reporterorganization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterorganization() {
        return reporterorganization;
    }

    /**
     * Sets the value of the reporterorganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterorganization(String value) {
        this.reporterorganization = value;
    }

    /**
     * Gets the value of the reporterdepartment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterdepartment() {
        return reporterdepartment;
    }

    /**
     * Sets the value of the reporterdepartment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterdepartment(String value) {
        this.reporterdepartment = value;
    }

    /**
     * Gets the value of the reporterstreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterstreet() {
        return reporterstreet;
    }

    /**
     * Sets the value of the reporterstreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterstreet(String value) {
        this.reporterstreet = value;
    }

    /**
     * Gets the value of the reportercity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportercity() {
        return reportercity;
    }

    /**
     * Sets the value of the reportercity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportercity(String value) {
        this.reportercity = value;
    }

    /**
     * Gets the value of the reporterstate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterstate() {
        return reporterstate;
    }

    /**
     * Sets the value of the reporterstate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterstate(String value) {
        this.reporterstate = value;
    }

    /**
     * Gets the value of the reporterpostcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterpostcode() {
        return reporterpostcode;
    }

    /**
     * Sets the value of the reporterpostcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterpostcode(String value) {
        this.reporterpostcode = value;
    }

    /**
     * Gets the value of the reportercountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportercountry() {
        return reportercountry;
    }

    /**
     * Sets the value of the reportercountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportercountry(String value) {
        this.reportercountry = value;
    }

    /**
     * Gets the value of the qualification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualification() {
        return qualification;
    }

    /**
     * Sets the value of the qualification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualification(String value) {
        this.qualification = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
