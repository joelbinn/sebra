package se.inera.biverkningsrapportering;

public enum Status {
    DRAFT, SUBMITTED
}
