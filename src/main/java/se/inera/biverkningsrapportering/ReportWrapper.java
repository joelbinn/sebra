package se.inera.biverkningsrapportering;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportType;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ReportWrapper {

    @JsonProperty("_id")
    @Id
    private String reportId;

    private ProcessIndividualCaseSafetyReportType report;
    private Status status;

    ReportWrapper() {
    }

    ReportWrapper(String reportId, Status status, ProcessIndividualCaseSafetyReportType report) {
        this.reportId = reportId;
        this.status = status;
        this.report = report;
    }

    String getReportId() {
        return reportId;
    }

    ProcessIndividualCaseSafetyReportType getReport() {
        return report;
    }

    void setReport(final ProcessIndividualCaseSafetyReportType report) {
        this.report = report;
    }

    Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    boolean isInDraftMode() {
        return Status.DRAFT == status;
    }

    @Override
    public String toString() {
        return "ReportWrapper{" +
                "reportId='" + reportId + '\'' +
                ", report=" + report +
                ", status=" + status +
                '}';
    }
}
