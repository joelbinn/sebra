package se.inera.biverkningsrapportering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceOperations;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ObjectFactory;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportResponseType;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportType;
import se.inera.biverkningsrapportering.gateway.SebraWSMessageCallback;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

//TODO: should @Component on class and @Autowire on constructor really be used when bean is created with new in Application?
//TODO cont:  gives compiler warning since SebraWSMessageCallback lacks Autowire-compatible constructor
//@Component
public class ProcessReportGateway {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessReportGateway.class);
    private final WebServiceOperations wsTemplate;
    private final SebraWSMessageCallback soapHandler;

    //@Autowired
    public ProcessReportGateway(WebServiceOperations wsTemplate, SebraWSMessageCallback soapHandler)
            throws JAXBException {
        this.wsTemplate = wsTemplate;
        this.soapHandler = soapHandler;
    }

    ProcessIndividualCaseSafetyReportResponseType send(ProcessIndividualCaseSafetyReportType report) throws JAXBException, IOException, XMLStreamException {
        LOG.debug("Report submitted: " + report);
        String messageNumb = report.getIndividualCaseSafetyReport().getIchicsr().getIchicsrmessageheader().getMessagenumb();

        LOG.info("Sending report with messageNumb {} to processing ", messageNumb);
        final WebServiceMessageCallback wsCallback = soapHandler.withMessageNumb(messageNumb);
        Object responseObject = wsTemplate.marshalSendAndReceive(toJaxbElement(report), wsCallback);

        ProcessIndividualCaseSafetyReportResponseType response = toP40Type(responseObject);
        LOG.debug("Recevied response from process call: [result code: " + response.getResultCode() + ", result text:" + response.getResultText() + "]");
        return response;
    }

    private ProcessIndividualCaseSafetyReportResponseType toP40Type(Object wsRespose) {
        final JAXBElement jaxbElement = JAXBElement.class.cast(wsRespose);
        return ProcessIndividualCaseSafetyReportResponseType.class.cast(jaxbElement.getValue());
    }

    private Object toJaxbElement(final ProcessIndividualCaseSafetyReportType report) {
        return new ObjectFactory().createProcessIndividualCaseSafetyReport(report);
    }

}
