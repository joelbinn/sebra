package se.inera.biverkningsrapportering;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
@ConfigurationProperties("sebra")
public class ApplicationSettings {

    private static final Predicate<String> isEmpty = str -> str == null || str.trim().isEmpty();

    private String processIndividualCaseSafetyReportTypeUrl;
    private String processIndividualCaseSafetyReportLogicalAddress;

    @Value("${server.ssl.enabled?:false}")
    private boolean sslEnabled;

    public void setKeystore(String keystore) {
        System.setProperty("javax.net.ssl.keyStore", keystore);
    }

    public void setKeystorePassword(String keystorePassword) {
        System.setProperty("javax.net.ssl.keyStorePassword", keystorePassword);
    }

    public void setTruststore(String truststore) {
        System.setProperty("javax.net.ssl.trustStore", truststore);
    }

    public void setTruststorePassword(String truststorePassword) {
        System.setProperty("javax.net.ssl.trustStorePassword", truststorePassword);
    }

    public String getProcessIndividualCaseSafetyReportTypeUrl() {
        return processIndividualCaseSafetyReportTypeUrl;
    }

    public void setProcessIndividualCaseSafetyReportTypeUrl(String processIndividualCaseSafetyReportTypeUrl) {
        this.processIndividualCaseSafetyReportTypeUrl = processIndividualCaseSafetyReportTypeUrl;
    }

    public String getProcessIndividualCaseSafetyReportLogicalAddress() {
        return processIndividualCaseSafetyReportLogicalAddress;
    }

    public void setProcessIndividualCaseSafetyReportLogicalAddress(String processIndividualCaseSafetyReportLogicalAddress) {
        this.processIndividualCaseSafetyReportLogicalAddress = processIndividualCaseSafetyReportLogicalAddress;
    }

    void setSslEnabled(final boolean sslEnabled) {
        this.sslEnabled = sslEnabled;
    }

    void validate() {
        if (isTLSEnabled()) {
            failIfEmpty(System.getProperty("javax.net.ssl.keyStore"), "keystore");
            failIfEmpty(System.getProperty("javax.net.ssl.keyStorePassword"), "keystorePassword");
            failIfEmpty(System.getProperty("javax.net.ssl.trustStore"), "truststore");
            failIfEmpty(System.getProperty("javax.net.ssl.trustStorePassword"), "truststorePassword");
        }
    }

    private boolean isTLSEnabled() {
        return sslEnabled || callingWSOverSSL();
    }

    private void failIfEmpty(final String setting, final String propertyName) {
        if (isEmpty.test(setting)) {
            throw new IllegalStateException(propertyName + " property required but not set.");
        }
    }

    private boolean callingWSOverSSL() {
        return processIndividualCaseSafetyReportTypeUrl != null && processIndividualCaseSafetyReportTypeUrl.startsWith("https");
    }

}