package se.inera.biverkningsrapportering;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportType;

/**
 * For now, test reports are written to db at application startup, hence this class is in main rather than test dir
 * Used from both Application main class and tests
 * //TODO: remove/move to test dir once reports are created via createDraft soap service
 */
@Service
public class TestReportWrapperFactory {

    private IdGenerator idGenerator;
    private TestXmlReportReader testXmlReportReader;

    @Autowired
    TestReportWrapperFactory(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
        testXmlReportReader = new TestXmlReportReader();
    }

    ReportWrapper getReportWrapperWithUniqueIdUsingPayload(String xmlFilePath) throws Exception {
        ProcessIndividualCaseSafetyReportType report = testXmlReportReader.getReportFromXml(xmlFilePath);
        String reportId = idGenerator.getUniqueId();
        updateIdRelatedPayloadFields(report, reportId);
        updateParentTypeToInternalClass(report);
        ReportWrapper reportWrapper = new ReportWrapper(reportId, Status.DRAFT, report);
        return reportWrapper;
    }

    protected void updateParentTypeToInternalClass(ProcessIndividualCaseSafetyReportType report) {
        String smarttagAsEscapedXml = report.getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().getPatient().getParent().getParentmedicalrelevanttext();
        ParentTypeInternal internalParent = new ParentTypeInternal();
        internalParent.setParentmedicalrelevanttext(smarttagAsEscapedXml);
        report.getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().getPatient().setParent(internalParent);
    }

    private void updateIdRelatedPayloadFields(ProcessIndividualCaseSafetyReportType testReport, String reportId) {
        String messageNumb = "SE-SEBRA-" + reportId;
        testReport.getIndividualCaseSafetyReport().getIchicsr().getIchicsrmessageheader().setMessagenumb(messageNumb);
        testReport.getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().setSafetyreportid(messageNumb);
        testReport.getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().setCompanynumb(messageNumb);
    }
}
