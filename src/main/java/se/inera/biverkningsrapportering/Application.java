package se.inera.biverkningsrapportering;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mongodb.DBCollection;
import org.mongojack.JacksonDBCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.xml.sax.SAXException;
import riv.lv.reporting.pharmacovigilance.v1.ParentType;
import se.inera.biverkningsrapportering.gateway.SebraWSMessageCallback;
import se.inera.biverkningsrapportering.gateway.SoapFaultMessageHandler;
import se.inera.biverkningsrapportering.validation.SchemaValidator;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@SpringBootApplication
@ComponentScan({"se.inera.biverkningsrapportering"})
public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessReportGateway.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        LOG.info("--- SEBRA application is started ---");
    }

    public Application(ApplicationSettings applicationSettings) {
        applicationSettings.validate();
    }

    //TODO: remove below when temporary load of test data is not needed on app init
    @Bean
    ApplicationRunner init(ReportRepository reportRepository, TestReportWrapperFactory testReportWrapperFactory) {
        return args -> {
            LOG.debug("Application.init: writing (wrapped) test report to db");
            ReportWrapper reportWrapper = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
            reportRepository.save(reportWrapper);
            reportWrapper = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_olle.xml");
            reportRepository.save(reportWrapper);
            LOG.debug("Reading all reports from db");
            List<ReportWrapper> allFromDb = reportRepository.findAll();
            allFromDb.stream()
                    .map(reportWrapperFromDb -> "Found report in db with reportId: " + reportWrapperFromDb.getReportId())
                    .forEach(LOG::debug);
        };
    }

    @Bean
    public JacksonDBCollection getJacksonDBCollection(MongoOperations mongoOperations) {
        DBCollection dbCollection = mongoOperations.getCollection(ReportWrapper.class.getSimpleName());
        return JacksonDBCollection.wrap(dbCollection, ReportWrapper.class, String.class, getSmarttagObjectMapper());
    }

    @Bean(name = "p32ResponderSchema")
    public Schema getSchema() throws SAXException {
        URL resource = getClass().getClassLoader().getResource("schemas/interactions/ProcessIndividualCaseSafetyReportInteraction/ProcessIndividualCaseSafetyReportResponder_1.0.xsd");
        return SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(resource);
    }

    @Bean
    @Primary
    public ProcessReportGateway processReportGateway(final ApplicationSettings appSettings,
                                                     final WebServiceTemplate wsTemplate)
            throws JAXBException, SAXException {
        wsTemplate.setDefaultUri(appSettings.getProcessIndividualCaseSafetyReportTypeUrl());

        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setContextPath("riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1");
        wsTemplate.setMarshaller(jaxb2Marshaller);
        wsTemplate.setUnmarshaller(jaxb2Marshaller);

        final String logicalAddress = appSettings.getProcessIndividualCaseSafetyReportLogicalAddress();
        String wsURL = appSettings.getProcessIndividualCaseSafetyReportTypeUrl();
        LOG.debug("Creating ProcessReportGateway for url {} and logicalAddress: {}", wsURL, logicalAddress);

        return new ProcessReportGateway(wsTemplate, new SebraWSMessageCallback(logicalAddress));
    }

    @Bean
    public WebServiceTemplate webServiceTemplate(@Qualifier("p32ResponderSchema") Schema p32ResponderSchema) {
        final WebServiceTemplate wsTemplate = new WebServiceTemplate();
        final SchemaValidator schemaValidator = SchemaValidator.forSchema(p32ResponderSchema);
        final PayloadValidationInterceptor payloadValidationInterceptor = new PayloadValidationInterceptor(schemaValidator);
        final ClientInterceptor[] interceptors = new ClientInterceptor[]{payloadValidationInterceptor};
        wsTemplate.setInterceptors(interceptors);

        wsTemplate.setFaultMessageResolver(new SoapFaultMessageHandler()); // Use our custom FaultMessageResolver

        return wsTemplate;
    }

    ObjectMapper getSmarttagObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(getSmarttagModule());
        return mapper;
    }

    @Bean
    public Module getSmarttagModule() {
        SimpleModule smarttagModule = new SimpleModule("smarttagModule");
        smarttagModule.addDeserializer(ParentType.class, new JsonDeserializer<ParentType>() {
            @Override
            public ParentType deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
                ObjectCodec codec = jp.getCodec();
                JsonNode node = codec.readTree(jp);
                return codec.treeToValue(node, ParentTypeInternal.class);
            }
        });
        return smarttagModule;
    }
}
