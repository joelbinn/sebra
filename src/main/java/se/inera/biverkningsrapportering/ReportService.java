package se.inera.biverkningsrapportering;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.WriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportResponseType;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ResultCodeEnum;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
public class ReportService {

    private static final Logger LOG = LoggerFactory.getLogger(ReportService.class);
    private static final ResponseEntity<String> STATUS_CODE_204 = ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    private static final ResponseEntity<String> STATUS_CODE_205 = ResponseEntity.status(HttpStatus.RESET_CONTENT).build();
    private static final ResponseEntity<String> REPORT_NOT_FOUND = ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN).body("Report not found.");
    private static final Predicate<String> isEmptyString = str -> str == null || str.trim().isEmpty();

    private ReportRepository reportRepository;
    private ProcessReportGateway processReportGateway;

    @RequestMapping(method = GET, value = "/report", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.TEXT_HTML_VALUE})
    @ResponseBody
    public ResponseEntity getReportByStatus(@RequestParam(value = "status", required = false) String status) {
        if (isEmptyString.test(status)) {
            LOG.error("Received report request with no path param or query param, returning http status {}", HttpStatus.BAD_REQUEST);
            return badRequest("Use '/report/{reportId}' or /report?status={status}");
        }

        LOG.info("Requesting reports with status {}", status);
        //TODO: catch IllegalArgumentException for unknown status string and explicitly return http error?
        Status statusEnumValue = Status.valueOf(status.toUpperCase());
        List<String> reportIds = reportRepository.getReportIdsByStatus(statusEnumValue);

        LOG.info("Returning {} reports", reportIds.size());
        return okResponse(new ApiResponse(reportIds));
    }

    @Autowired
    ReportService(ReportRepository reportRepository, ProcessReportGateway processReportGateway) {
        this.reportRepository = reportRepository;
        this.processReportGateway = processReportGateway;
    }

    @RequestMapping(method = GET, value = "/report/{reportId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity getReportById(@PathVariable("reportId") String reportId) throws JsonProcessingException {
        LOG.info("Requesting report with ID {}", reportId);

        Optional<ReportWrapper> reportWrapperOnRecord = reportRepository.findOneById(reportId);
        ReportWrapper reportWrapper;
        if (reportWrapperOnRecord.isPresent()) {
            reportWrapper = reportWrapperOnRecord.get();
        } else {
            LOG.info("No report with id {} found, returning {}", reportId, REPORT_NOT_FOUND);
            return REPORT_NOT_FOUND;
        }

        LOG.info("Returning report with id: {}", reportId);
        LOG.debug("Report to return: " + getAsString(reportWrapper));
        return okResponse(reportWrapper);
    }

    private String getAsString(ReportWrapper reportWrapper) throws JsonProcessingException {
        ObjectMapper toJsonMapper = new ObjectMapper();
        return toJsonMapper.writeValueAsString(reportWrapper);
    }

    @RequestMapping(method = PUT, value = "/report/{reportId}", consumes = {APPLICATION_JSON_VALUE, APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public ResponseEntity<String> updateReport(
            @PathVariable("reportId") String reportId,
            @RequestBody ReportWrapper incomingReport,
            @RequestParam(value = "submit", defaultValue = "false") boolean is2BSubmitted
    ) throws JsonProcessingException {
        LOG.debug("Incoming report: {}", getAsString(incomingReport));
        failIfNotValid(incomingReport);
        // TODO: VALIDATION: dbReport and incomingReport are assumed to have the same id (which is not guaranteed).
        //      i.e. validate that reportId of incoming report (the put payload) is equal to the parameter reportId

        Optional<ReportWrapper> reportOnRecord = reportRepository.findOneById(reportId);
        ReportWrapper dbReport;
        if (reportOnRecord.isPresent()) {
            dbReport = reportOnRecord.get();
        } else {
            return REPORT_NOT_FOUND;
        }

        ResponseEntity<String> response = STATUS_CODE_204;

        if (dbReport.isInDraftMode()) {
            //TODO: save report before (possible) send (too)?
            if (is2BSubmitted) {
                try {
                    // Submit to LV
                    ProcessIndividualCaseSafetyReportResponseType wsResponse = submitReport(incomingReport);
                    //Note: as long as the report is sent (i.e. passes sebra internal validations),
                    //status must be updated and report NOT allowed to be re-submitted,
                    // even in case of error response from ws
                    // (since correlation id is set to messagenumb and must be unique per request)
                    // i.e. if re-submission is to be allowed, handling of corr id has to be adjusted
                    incomingReport.setStatus(Status.SUBMITTED);
                    response = isSuccessfulResponse(wsResponse) ? STATUS_CODE_205 : responseWithError(wsResponse);
                } catch (Exception e) {
                    String errorMessage = "Could not submit report " + reportId + "";
                    LOG.error(errorMessage, e);
                    response = internalServerError(errorMessage);
                }
            }

            WriteResult saveResult = reportRepository.save(incomingReport);
            if (!saveResult.wasAcknowledged()) {
                String errorMessage = "Could not persist report " + reportId + "";
                LOG.error(errorMessage);
                response = internalServerError(errorMessage);
            }

        } else {
            LOG.warn("Report {} has status {}, no action will be taken.", reportId, dbReport.getStatus());
            response = STATUS_CODE_205;
        }

        return response;
    }

    private void failIfNotValid(final ReportWrapper incomingReport) {
        if (incomingReport.getReport() == null || isEmptyString.test(incomingReport.getReportId())) {
            throw new IllegalArgumentException("Your request contains invalid report.");
        }
    }

    @ExceptionHandler(IllegalArgumentException.class)
    private ResponseEntity<String> invalidArgument(IllegalArgumentException iae) {
        return badRequest(iae.getMessage());
    }

    private ResponseEntity<String> responseWithError(final ProcessIndividualCaseSafetyReportResponseType wsResponse) {
        return internalServerError(wsResponse.getResultText());
    }

    private ResponseEntity<String> internalServerError(String errorMessage) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.TEXT_PLAIN).body(errorMessage);
    }

    private boolean isSuccessfulResponse(final ProcessIndividualCaseSafetyReportResponseType wsResponse) {
        return ResultCodeEnum.OK == wsResponse.getResultCode();
    }

    private ResponseEntity<ReportWrapper> okResponse(ReportWrapper resource) {
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8).body(resource);
    }

    private ResponseEntity<ApiResponse> okResponse(final ApiResponse apiResponse) {
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8).body(apiResponse);
    }

    private ProcessIndividualCaseSafetyReportResponseType submitReport(final @RequestBody ReportWrapper incomingReport) throws JAXBException, IOException, XMLStreamException {
        LOG.debug("Submitting report");
        ProcessIndividualCaseSafetyReportResponseType wsResponse = processReportGateway.send(incomingReport.getReport());
        log(wsResponse);
        return wsResponse;
    }

    private void log(final ProcessIndividualCaseSafetyReportResponseType result) {
        ObjectMapper toJsonMapper = new ObjectMapper();
        String resultAsString = null;
        try {
            resultAsString = toJsonMapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            LOG.warn("Could not convert Object to JSON: ", e.toString());
        }
        if (ResultCodeEnum.OK == result.getResultCode()) {
            LOG.info("Successfully submitted report.");
        } else {
            LOG.warn("Failed submitting report: {}", resultAsString);
        }
    }

    private ResponseEntity<String> badRequest(final String message) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(message);
    }

    /**
     * When serialized by JSON converter, the client will see a response such as:
     * <pre>{"reportIds":["id1","id2","id3"]}</pre>
     */
    public class ApiResponse {

        List<String> reportIds;

        ApiResponse(List<String> reportIds) {
            this.reportIds = reportIds;
        }

        public List<String> getReportIds() {
            return reportIds;
        }
    }
}
