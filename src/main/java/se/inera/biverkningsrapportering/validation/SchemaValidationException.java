package se.inera.biverkningsrapportering.validation;

import org.springframework.ws.client.WebServiceClientException;

public class SchemaValidationException extends WebServiceClientException {

    public SchemaValidationException(final String msg) {
        super(msg);
    }
}
