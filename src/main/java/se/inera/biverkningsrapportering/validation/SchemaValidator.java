package se.inera.biverkningsrapportering.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.validation.Schema;
import java.io.IOException;
import java.io.InputStream;

//TODO: For logging purposes, we need some intelligent way of identifying what is it we're validating (what schema file, what content)

public class SchemaValidator {
    private static final Logger LOG = LoggerFactory.getLogger(SchemaValidator.class);
    private final SAXParserFactory spFactory;
    private final XmlSourceTransformer sourceTransformer;

    public static final SchemaValidator forSchema(Schema schema) {
        return new SchemaValidator(SAXParserFactory.newInstance(), new XmlSourceTransformer(), schema);
    }

    public SchemaValidator(final SAXParserFactory saxParserFactory, final XmlSourceTransformer sourceTransformer, Schema schema) {
        this.spFactory = saxParserFactory;
        this.sourceTransformer = sourceTransformer;
        this.spFactory.setSchema(schema);
        saxParserFactory.setNamespaceAware(true);
    }

    /**
     *
     * @param source the XML source to be validated.
     * @return <code>true</code> if source validates.
     */
    public boolean validates(Source source) {
        try {
            InputStream sourceStream = sourceTransformer.toInputStream(source);
            return validates(sourceStream);
        } catch (TransformerException e) {
            LOG.error("Error transforming source: {}", e.toString());
        }
        return false;
    }

    boolean validates(final InputStream toValidate) {
        try {
            SAXParser parser = spFactory.newSAXParser();
            final ParserHandler parseHandler = new ParserHandler();
            parser.parse(toValidate, parseHandler);
            return parseHandler.isErrorFree();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.warn("Error during validation: {}", e.toString());
        }

        return false;
    }

    private class ParserHandler extends DefaultHandler {
        boolean isErrorFree = true;

        boolean isErrorFree() {
            return isErrorFree;
        }

        @Override
        public void warning(final SAXParseException e) throws SAXException {
            LOG.warn("Validation: {}", e.toString());
        }

        @Override
        public void error(final SAXParseException e) throws SAXException {
            isErrorFree = false;
            LOG.error("XML not valid. Cause: {}", e.toString());
        }

        @Override
        public void fatalError(final SAXParseException e) throws SAXException {
            isErrorFree = false;
            LOG.error("XML not valid. Cause: {}", e.toString());
        }
    }
}
