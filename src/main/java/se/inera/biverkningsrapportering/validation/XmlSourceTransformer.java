package se.inera.biverkningsrapportering.validation;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class XmlSourceTransformer {

    InputStream toInputStream(final Source source) throws TransformerException {
        final Transformer t = TransformerFactory.newInstance().newTransformer();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final Result result = new StreamResult(baos);
        t.transform(source, result);
        return new ByteArrayInputStream(baos.toByteArray());
    }
}
