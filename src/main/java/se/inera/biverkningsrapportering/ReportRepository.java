package se.inera.biverkningsrapportering;

import com.mongodb.WriteResult;
import org.mongojack.DBCursor;
import org.mongojack.DBProjection;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReportRepository {
    private final JacksonDBCollection<ReportWrapper, String> dbCollection;

    //TODO: List<ReportWrapper> findByHsaIdAndPersonIdentifier(...);?

    @Autowired
    public ReportRepository(JacksonDBCollection<ReportWrapper, String> dbCollection) {
        this.dbCollection = dbCollection;
    }

    WriteResult save(ReportWrapper sebraReport) {
        org.mongojack.WriteResult<ReportWrapper, String> saveResult = dbCollection.save(sebraReport);
        return new WriteResult(saveResult.getN(), saveResult.isUpdateOfExisting(), saveResult.getSavedId());
    }

    Optional<ReportWrapper> findOneById(String reportId) {
        ReportWrapper objectFromDb = dbCollection.findOneById(reportId);
        return Optional.ofNullable(objectFromDb);
    }

    List<ReportWrapper> findAll() {
        DBCursor<ReportWrapper> resultCursor = dbCollection.find();
        List<ReportWrapper> reportWrappers = resultCursor.toArray();
        return reportWrappers;
    }

    List<String> getReportIdsByStatus(Status status) {
        DBQuery.Query getByStatus = DBQuery.is("status", status);
        DBProjection.ProjectionBuilder onlyId = DBProjection.include("_id");
        List<ReportWrapper> matchingReportWrappers = dbCollection.find(getByStatus, onlyId).toArray();
        List<String> matchingReportIds = matchingReportWrappers.stream().map(reportWrapper -> reportWrapper.getReportId()).collect(Collectors.toList());
        return matchingReportIds;
    }

    void drop() {
        dbCollection.drop();
    }

}
