package se.inera.biverkningsrapportering;

import org.springframework.data.annotation.Id;

public class SequenceId {

    @Id
    private String id;

    private int seq;

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
