package se.inera.biverkningsrapportering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import se.inera.biverkningsrapportering.validation.SchemaValidationException;
import se.inera.biverkningsrapportering.validation.SchemaValidator;

import javax.xml.transform.Source;

@Configurable
public class PayloadValidationInterceptor implements ClientInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(PayloadValidationInterceptor.class);
    private final SchemaValidator schemaValidator;

    @Autowired
    public PayloadValidationInterceptor(final SchemaValidator schemaValidator) {
        this.schemaValidator = schemaValidator;
    }

    @Override
    public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
        LOG.trace("PayloadValidationInterceptor.handleRequest() START");
        schemaValidate(messageContext);
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
        LOG.trace("PayloadValidationInterceptor.handleResponse() START");
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
        LOG.trace("PayloadValidationInterceptor.handleFault() START");
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Exception ex) throws WebServiceClientException {
        LOG.trace("PayloadValidationInterceptor.afterCompletion() START");
    }

    private void schemaValidate(final MessageContext messageContext) {
        Source p26Report = messageContext.getRequest().getPayloadSource(); // == ProcessIndividualCaseSafetyReport
        if (!schemaValidator.validates(p26Report)) {
            throw new SchemaValidationException("Validation failed. Message cannot be sent.");
        }
    }
}
