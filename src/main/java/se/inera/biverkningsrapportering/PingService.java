package se.inera.biverkningsrapportering;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class PingService {

    @RequestMapping(method = GET, value = "/ping", produces = "application/json")
    @ResponseBody
    public String ping() {
        return "{\"response\":\"Pong!\"}";
    }

}
