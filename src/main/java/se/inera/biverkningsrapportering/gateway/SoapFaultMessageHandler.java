package se.inera.biverkningsrapportering.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.client.core.SoapFaultMessageResolver;

import java.io.IOException;

/**
 * For handling Soap fault response messages.
 */
public class SoapFaultMessageHandler extends SoapFaultMessageResolver {

    private static final Logger LOG = LoggerFactory.getLogger(SoapFaultMessageHandler.class);

    @Override
    public void resolveFault(final WebServiceMessage message) throws IOException {
        try {
            super.resolveFault(message);
        } catch (SoapFaultClientException soapEx) {
            LOG.error("SOAP FAULT: {} - {}", soapEx.getFaultCode(), soapEx.getFaultStringOrReason());
        }
    }
}
