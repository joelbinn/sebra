package se.inera.biverkningsrapportering.gateway;

import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpUrlConnection;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class SebraWSMessageCallback implements WebServiceMessageCallback {

    private static final QName LOGICAL_ADDRESS_QNAME = new QName("urn:riv:itintegration:registry:1", "LogicalAddress");
    private static final String SOAP_ACTION = "urn:riv:lv:reporting:pharmacovigilance:ProcessIndividualCaseSafetyReportResponder:1:ProcessIndividualCaseSafetyReport";

    private final String logicalAddress;
    private String messageNumb;

    public SebraWSMessageCallback(final String logicalAddress) {
        this.logicalAddress = logicalAddress;
    }

    @Override
    public void doWithMessage(final WebServiceMessage message) throws IOException, TransformerException {
        getWsConnection().addRequestHeader("x-skltp-correlation-id", messageNumb);
        SoapMessage soapMessage = (SoapMessage) message;
        soapMessage.setSoapAction(SOAP_ACTION);
        addLogicalAddressAsSoapHeader(soapMessage.getSoapHeader());
    }

    HttpUrlConnection getWsConnection() {
        final TransportContext transportContext = TransportContextHolder.getTransportContext();
        return (HttpUrlConnection) transportContext.getConnection();
    }

    public SebraWSMessageCallback withMessageNumb(final String messageNumb) {
        this.messageNumb = messageNumb;
        return this;
    }

    private void addLogicalAddressAsSoapHeader(final SoapHeader soapHeader) {
        SoapHeaderElement soapHeaderElement = soapHeader.addHeaderElement(LOGICAL_ADDRESS_QNAME);
        soapHeaderElement.setText(logicalAddress);
    }
}
