package se.inera.biverkningsrapportering;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class IdGenerator {

    private MongoOperations mongo;

    private String SEQUENCE_NAME = "reportId";

    @Autowired
    IdGenerator(MongoOperations mongo) {
        this.mongo = mongo;
    }

    public String getUniqueId() {
        return getNextSequenceNumberFromMongo();
    }

    //Note: requires that the collection SequenceId exists in the mongo db instance,
    // and that a document with id SEQUENCE_NAME exists in that collection
    //TODO: should we create sequence if it does not exist..?
    private String getNextSequenceNumberFromMongo() {
        SequenceId counter = mongo.findAndModify(
                query(where("_id").is(SEQUENCE_NAME)),
                new Update().inc("seq", 1),
                options().returnNew(true).upsert(true),
                SequenceId.class);
        return Integer.toString(counter.getSeq());
    }
}
