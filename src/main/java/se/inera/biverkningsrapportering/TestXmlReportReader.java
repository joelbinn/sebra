package se.inera.biverkningsrapportering;

import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;

public class TestXmlReportReader {

    ProcessIndividualCaseSafetyReportType getReportFromXml(String xmlFilePath) throws Exception {
        JAXBContext jc = JAXBContext.newInstance(ProcessIndividualCaseSafetyReportType.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        InputStream xml = getClass().getClassLoader().getResourceAsStream(xmlFilePath);
        JAXBElement<ProcessIndividualCaseSafetyReportType> jaxbElement = (JAXBElement<ProcessIndividualCaseSafetyReportType>) unmarshaller.unmarshal(xml);
        ProcessIndividualCaseSafetyReportType report = jaxbElement.getValue();
        return report;
    }

}
