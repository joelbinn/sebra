package se.inera.biverkningsrapportering;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.cxf.staxutils.transform.IgnoreNamespacesWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import riv.lv.reporting.pharmacovigilance.v1.ParentType;
import smarttags.ObjectFactory;
import smarttags.SmartTagType;

import javax.xml.bind.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

public class ParentTypeInternal extends ParentType {

    private static final Logger LOG = LoggerFactory.getLogger(ParentTypeInternal.class);

    private SmartTagType smarttag;

    public SmartTagType getSmarttag() {
        return smarttag;
    }

    public void setSmarttag(SmartTagType smarttag) throws JAXBException, XMLStreamException {
        this.smarttag = smarttag;
        this.parentmedicalrelevanttext = getSmarttagContentAsEscapedXml(smarttag);
    }

    @Override
    @JsonIgnore
    public String getParentmedicalrelevanttext() {
        return parentmedicalrelevanttext;
    }

    @Override
    @JsonIgnore
    public void setParentmedicalrelevanttext(String value) {
        try {
            this.parentmedicalrelevanttext = value;
            this.smarttag = createSmarttagObjectFromEscapedXml(value);
        } catch (Exception e) {
            LOG.error("Error creating smarttag from escaped xml", e);
            //TODO: ok to throw exception from pojo?
            throw new RuntimeException(e);
        }
    }

    protected SmartTagType createSmarttagObjectFromEscapedXml(String value) throws JAXBException, XMLStreamException, UnsupportedEncodingException {
        String innerXml = StringEscapeUtils.unescapeXml(value);
        String xml = "<smarttag>" + innerXml + "</smarttag>";
        JAXBContext context = JAXBContext.newInstance(smarttags.ObjectFactory.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        JAXBElement<SmartTagType> smarttagElem = (JAXBElement<SmartTagType>) unmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes("UTF-8")));
        return smarttagElem.getValue();
    }

    protected String getSmarttagContentAsEscapedXml(SmartTagType smartTagObject) throws JAXBException, XMLStreamException {
        JAXBContext context = JAXBContext.newInstance(smarttags.SmartTagType.class);
        Marshaller marshaller = context.createMarshaller();

        ByteArrayOutputStream smartTagOutputStream = new ByteArrayOutputStream();
        XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newFactory().createXMLStreamWriter(smartTagOutputStream);
        IgnoreNamespacesWriter noNamespaceWriter = new IgnoreNamespacesWriter(xmlStreamWriter);

        marshaller.marshal(getJaxbElement(smartTagObject), noNamespaceWriter);
        String smartTagXml = new String(smartTagOutputStream.toByteArray());

        int startIndex = smartTagXml.indexOf("<smarttag>") + "<smarttag>".length();
        int endIndex = smartTagXml.indexOf("</smarttag>");
        String innerXml = smartTagXml.substring(startIndex, endIndex);

        String escapedInnerXml = StringEscapeUtils.escapeXml10(innerXml);
        return escapedInnerXml;
    }

    private Object getJaxbElement(SmartTagType smartTagObject) {
        return new ObjectFactory().createSmarttag(smartTagObject);
    }

}
