//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.10.31 at 01:56:02 PM CET 
//


package smarttags;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SmartTagType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SmartTagType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="patientfirstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="patientlastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="patientidentitynumber" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9]{8}[0-9A-Zptf]{4}"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="reporterhsaid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reporterunithsaid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="healthcarecareserviceprovider" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reporterphone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="municipalitycode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countycode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hsatitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="caretype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="management" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="drug" type="{}DrugType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="document" type="{}DocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SmartTagType", propOrder = {
    "patientfirstname",
    "patientlastname",
    "patientidentitynumber",
    "reporterhsaid",
    "reporterunithsaid",
    "healthcarecareserviceprovider",
    "reporterphone",
    "municipalitycode",
    "countycode",
    "hsatitle",
    "caretype",
    "management",
    "drug",
    "document"
})
public class SmartTagType {

    protected String patientfirstname;
    protected String patientlastname;
    protected String patientidentitynumber;
    protected String reporterhsaid;
    protected String reporterunithsaid;
    protected String healthcarecareserviceprovider;
    protected String reporterphone;
    protected String municipalitycode;
    protected String countycode;
    protected String hsatitle;
    protected String caretype;
    protected String management;
    protected List<DrugType> drug;
    protected List<DocumentType> document;

    /**
     * Gets the value of the patientfirstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientfirstname() {
        return patientfirstname;
    }

    /**
     * Sets the value of the patientfirstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientfirstname(String value) {
        this.patientfirstname = value;
    }

    /**
     * Gets the value of the patientlastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientlastname() {
        return patientlastname;
    }

    /**
     * Sets the value of the patientlastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientlastname(String value) {
        this.patientlastname = value;
    }

    /**
     * Gets the value of the patientidentitynumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientidentitynumber() {
        return patientidentitynumber;
    }

    /**
     * Sets the value of the patientidentitynumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientidentitynumber(String value) {
        this.patientidentitynumber = value;
    }

    /**
     * Gets the value of the reporterhsaid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterhsaid() {
        return reporterhsaid;
    }

    /**
     * Sets the value of the reporterhsaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterhsaid(String value) {
        this.reporterhsaid = value;
    }

    /**
     * Gets the value of the reporterunithsaid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterunithsaid() {
        return reporterunithsaid;
    }

    /**
     * Sets the value of the reporterunithsaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterunithsaid(String value) {
        this.reporterunithsaid = value;
    }

    /**
     * Gets the value of the healthcarecareserviceprovider property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthcarecareserviceprovider() {
        return healthcarecareserviceprovider;
    }

    /**
     * Sets the value of the healthcarecareserviceprovider property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthcarecareserviceprovider(String value) {
        this.healthcarecareserviceprovider = value;
    }

    /**
     * Gets the value of the reporterphone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReporterphone() {
        return reporterphone;
    }

    /**
     * Sets the value of the reporterphone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReporterphone(String value) {
        this.reporterphone = value;
    }

    /**
     * Gets the value of the municipalitycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMunicipalitycode() {
        return municipalitycode;
    }

    /**
     * Sets the value of the municipalitycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMunicipalitycode(String value) {
        this.municipalitycode = value;
    }

    /**
     * Gets the value of the countycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountycode() {
        return countycode;
    }

    /**
     * Sets the value of the countycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountycode(String value) {
        this.countycode = value;
    }

    /**
     * Gets the value of the hsatitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHsatitle() {
        return hsatitle;
    }

    /**
     * Sets the value of the hsatitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHsatitle(String value) {
        this.hsatitle = value;
    }

    /**
     * Gets the value of the caretype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaretype() {
        return caretype;
    }

    /**
     * Sets the value of the caretype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaretype(String value) {
        this.caretype = value;
    }

    /**
     * Gets the value of the management property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagement() {
        return management;
    }

    /**
     * Sets the value of the management property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagement(String value) {
        this.management = value;
    }

    /**
     * Gets the value of the drug property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the drug property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDrug().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DrugType }
     * 
     * 
     */
    public List<DrugType> getDrug() {
        if (drug == null) {
            drug = new ArrayList<DrugType>();
        }
        return this.drug;
    }

    /**
     * Gets the value of the document property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the document property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentType }
     * 
     * 
     */
    public List<DocumentType> getDocument() {
        if (document == null) {
            document = new ArrayList<DocumentType>();
        }
        return this.document;
    }

}
