package se.inera.biverkningsrapportering;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("autotest")
public class Application_IT {

    @LocalServerPort
    int localServerPort;

    @Before
    public void setUP() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = localServerPort;
    }

    @Test
    public void should_respond_to_ping_request() {
        given()
                .when()
                .get("/ping")
                .then()
                .assertThat()
                .statusCode(200)
                .contentType("application/json")
                .body("response", response -> equalTo("Pong!"));
    }

}
