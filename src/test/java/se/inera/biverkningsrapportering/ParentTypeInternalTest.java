package se.inera.biverkningsrapportering;

import org.junit.Test;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.xml.transform.StringResult;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ObjectFactory;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportType;
import smarttags.SmartTagType;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import static org.assertj.core.api.Assertions.assertThat;

public class ParentTypeInternalTest {

    @Test
    public void smarttagObjectCanBeCreatedFromEscapedXml() throws Exception {
        //given
        ParentTypeInternal parentTypeInternal = new ParentTypeInternal();
        ProcessIndividualCaseSafetyReportType report = new TestXmlReportReader().getReportFromXml("testdata/test_report_anna.xml");
        String parentmedicalrelevanttext = report.getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().getPatient().getParent().getParentmedicalrelevanttext();

        //when
        SmartTagType smarttagObject = parentTypeInternal.createSmarttagObjectFromEscapedXml(parentmedicalrelevanttext);

        //then
        assertThat(smarttagObject.getPatientfirstname()).isEqualTo("Anna");
    }

    @Test
    public void escapedXmlCanBeGeneratedFromSmarttagObject() throws JAXBException, XMLStreamException {
        //given
        ParentTypeInternal parentTypeInternal = new ParentTypeInternal();
        SmartTagType smartTagObject = new SmartTagType();
        smartTagObject.setPatientfirstname("Stefan");

        //when
        String escapedXml = parentTypeInternal.getSmarttagContentAsEscapedXml(smartTagObject);

        //then
        //TODO: validate
        assertThat(escapedXml).startsWith("&lt;");
        assertThat(escapedXml).endsWith("&gt;");
        assertThat(escapedXml).contains("Stefan");
    }

    @Test
    public void unmarshalOfReportWithInternalParentTypeGivesExpectedXml() throws Exception {
        //given
        ProcessIndividualCaseSafetyReportType reportToSend = new TestXmlReportReader().getReportFromXml("testdata/test_report_anna.xml");
        new TestReportWrapperFactory(null).updateParentTypeToInternalClass(reportToSend);

        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setContextPath("riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1");
        StringResult result = new StringResult();

        //when
        jaxb2Marshaller.marshal(toJaxbElement(reportToSend), result);
        String resultingXml = result.toString();

        //then
        assertThat(resultingXml).contains("parentmedicalrelevanttext>\n" +
                "                            &lt;patientfirstname&gt;Anna&lt;/patientfirstname&gt;");
    }

    private Object toJaxbElement(final ProcessIndividualCaseSafetyReportType report) {
        return new ObjectFactory().createProcessIndividualCaseSafetyReport(report);
    }


}
