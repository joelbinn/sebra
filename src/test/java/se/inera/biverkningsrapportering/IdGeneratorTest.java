package se.inera.biverkningsrapportering;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataMongoTest
@ContextConfiguration(classes = EmptyTestConfig.class)//used to NOT load full application context
@ActiveProfiles("autotest")
public class IdGeneratorTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    IdGenerator idGenerator;

    @Before
    public void setup() {
        idGenerator = new IdGenerator(mongoTemplate);
    }

    @Test
    public void idIsIncremented() {
        String id1 = idGenerator.getUniqueId();
        String id2 = idGenerator.getUniqueId();

        assertThat(Integer.parseInt(id2) - Integer.parseInt(id1)).isEqualTo(1);
    }
}
