package se.inera.biverkningsrapportering;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

public class ReportWrapperTest {

    private ObjectMapper jsonMapper = createJsonMapper();

    @Test
    public void expecting_correctly_deserialized_object() throws Exception {
        // Given:

        // When:
        ReportWrapper sampleReport = getSampleReport();

        // Then:
        assertThat(sampleReport.getReport()).hasNoNullFieldsOrProperties();
    }

    private ReportWrapper getSampleReport() throws java.io.IOException {
        URL sampleJson = getClass().getClassLoader().getResource("testdata/sample_report_1.json");

        return jsonMapper.readValue(sampleJson, ReportWrapper.class);
    }

    private ObjectMapper createJsonMapper() {
        return new Application(new ApplicationSettings()).getSmarttagObjectMapper();
    }
}