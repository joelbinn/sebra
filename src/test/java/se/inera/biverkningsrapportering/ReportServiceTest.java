package se.inera.biverkningsrapportering;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.WriteResult;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportResponseType;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ResultCodeEnum;

import java.io.InputStream;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.when;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class ReportServiceTest {
    private static String SAMPLE_REPORT_ID = "xXR-DQM4hwOyJfTfvo";

    private ObjectMapper jsonMapper = createJsonMapper();

    @Test
    public void should_return_http_500_when_WS_error_is_encountered() throws Exception {
// Given:
        final ReportWrapper sampleReport = sampleReport();
        final ReportRepository reportRepository = mock(ReportRepository.class);
        when(reportRepository.findOneById(anyString())).thenReturn(Optional.of(sampleReport));
        when(reportRepository.save(any())).thenReturn(goodResult());

        final ProcessReportGateway reportGW = mock(ProcessReportGateway.class);
        when(reportGW.send(any())).thenReturn(simulatedWSFailure());

        ReportService reportService = new ReportService(reportRepository, reportGW);

// When:
        ResponseEntity<String> actualResponse = reportService.updateReport("DON'T_CARE", sampleReport, true);

// Then:
        assertThat(actualResponse).satisfies(resp -> {
            assertThat(resp.getStatusCodeValue()).isEqualTo(500);
            assertThat(resp.getBody()).contains("BAD CODE!");
        });
    }

    @Test
    public void should_return_http_205_when_resubmitting_submitted_report() throws Exception {
// Given:
        final ReportWrapper sampleReport = sampleReport();
        sampleReport.setStatus(Status.SUBMITTED);

        final ReportRepository reportRepository = mock(ReportRepository.class);
        when(reportRepository.findOneById(anyString())).thenReturn(Optional.of(sampleReport));

        ReportService reportService = new ReportService(reportRepository, null);

// When:
        ResponseEntity<String> actualResponse = reportService.updateReport(sampleReport.getReportId(), sampleReport, true);

// Then:
        assertThat(actualResponse).satisfies(resp -> {
            assertThat(resp.getStatusCodeValue()).isEqualTo(205);
            assertThat(resp.getBody()).isNull();
        });
    }

    @Test
    public void already_submitted_report_should_not_resubmitted() throws Exception {
// Given:
        final ReportWrapper sampleReport = sampleReport();
        sampleReport.setStatus(Status.SUBMITTED);

        final ReportRepository reportRepository = mock(ReportRepository.class);
        final ProcessReportGateway reportGW = mock(ProcessReportGateway.class);

        when(reportRepository.findOneById(anyString())).thenReturn(Optional.of(sampleReport));

        ReportService reportService = new ReportService(reportRepository, null);

// When:
        reportService.updateReport(sampleReport.getReportId(), sampleReport, true);

// Then:
        verify(reportGW, never()).send(any());
    }

    @Test
    public void should_return_http_404_when_no_report_is_found() throws Exception {
// Given:
        final ReportWrapper sampleReport = sampleReport();
        final ReportRepository reportRepository = mock(ReportRepository.class);
        when(reportRepository.findOneById(anyString())).thenReturn(Optional.empty());

        ReportService reportService = new ReportService(reportRepository, null);

// When:
        ResponseEntity<String> actualResponse = reportService.updateReport("BLA-BLA", sampleReport, true);

// Then:
        assertThat(actualResponse).satisfies(resp -> {
            assertThat(resp.getStatusCodeValue()).isEqualTo(404);
            assertThat(resp.getBody()).isEqualTo("Report not found.");
        });
    }

    @Test
    public void should_query_on_status_on_valid_status_param() throws Exception {
// Given:
        final ReportRepository reportRepository = mock(ReportRepository.class);
        ReportService reportService = new ReportService(reportRepository, null);

// When:
        String draftWithMixedCase = "DrAfT"; // Also tests case-insensitivity
        reportService.getReportByStatus(draftWithMixedCase);

// Then:
        verify(reportRepository).getReportIdsByStatus(Status.DRAFT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_for_INvalid_status_param() throws Exception {
// Given:
        ReportService reportService = new ReportService(null, null);

// When:
        reportService.getReportByStatus("not-a-status");

// Then:
        //exception is thrown
    }

    @Ignore
    //TODO: implement equals in ReportWrapper and domain classes, and keep below test?
    //  otherwise, remove or update assertion
    public void should_update_draft_report_status_when_submitted() throws Exception {
// Given:
        final ReportWrapper sampleReport = sampleReport();
        final ReportRepository reportRepository = mock(ReportRepository.class);
        when(reportRepository.findOneById(anyString())).thenReturn(Optional.of(sampleReport));
        when(reportRepository.save(any())).thenReturn(goodResult());
        final ProcessReportGateway reportGW = mock(ProcessReportGateway.class);
        when(reportGW.send(any())).thenReturn(WSSuccess());

        ReportService reportService = new ReportService(reportRepository, reportGW);

// When:
        reportService.updateReport("BLA-BLA", sampleReport, true);

// Then:
        ReportWrapper sampleReportWithUpdatedStatus = sampleReport();
        sampleReportWithUpdatedStatus.setStatus(Status.SUBMITTED);
        verify(reportRepository).save(sampleReportWithUpdatedStatus);
    }


    private WriteResult goodResult() {
        return new WriteResult(1, true, SAMPLE_REPORT_ID);
    }

    private ProcessIndividualCaseSafetyReportResponseType simulatedWSFailure() {
        ProcessIndividualCaseSafetyReportResponseType resp = new ProcessIndividualCaseSafetyReportResponseType();
        resp.setResultCode(ResultCodeEnum.ERROR);
        resp.setResultText("BAD CODE!");
        return resp;
    }

    private ProcessIndividualCaseSafetyReportResponseType WSSuccess() {
        ProcessIndividualCaseSafetyReportResponseType resp = new ProcessIndividualCaseSafetyReportResponseType();
        resp.setResultCode(ResultCodeEnum.OK);
        return resp;
    }

    private ReportWrapper sampleReport() throws Exception {
        InputStream resource = getClass().getClassLoader().getResourceAsStream("testdata/sample_report_1.json");
        return jsonMapper.readValue(resource, ReportWrapper.class);
    }

    private ObjectMapper createJsonMapper() {
        return new Application(new ApplicationSettings()).getSmarttagObjectMapper();
    }

}
