package se.inera.biverkningsrapportering;

import org.junit.Test;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceOperations;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportResponseType;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportType;
import riv.lv.reporting.pharmacovigilance.v1.ICHICSRMessageHeaderType;
import riv.lv.reporting.pharmacovigilance.v1.ICHICSRType;
import riv.lv.reporting.pharmacovigilance.v1.IndividualCaseSafetyReportType;
import se.inera.biverkningsrapportering.gateway.SebraWSMessageCallback;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ProcessReportGatewayTest {

    @Test
    public void assert_reportGateway_uses_SebraWSMessageCallback_on_soap_request() throws Exception {
        // Given:
        final WebServiceOperations wsTempl = mock(WebServiceOperations.class);
        final SebraWSMessageCallback soapHandler = mock(SebraWSMessageCallback.class);
        final ProcessReportGateway reportGateway = new ProcessReportGateway(wsTempl, soapHandler);
        when(soapHandler.withMessageNumb(anyString())).thenReturn(soapHandler);
        when(wsTempl.marshalSendAndReceive(any(Object.class), isA(WebServiceMessageCallback.class))).thenReturn(dummyReportResponse());

        // When:
        reportGateway.send(testReport());

        // Then:
        verify(soapHandler).withMessageNumb("ONE_2_THREE_4_NUM");
        verify(wsTempl).marshalSendAndReceive(any(Object.class), isA(SebraWSMessageCallback.class));
        verify(wsTempl).marshalSendAndReceive(any(Object.class), eq(soapHandler));
    }

    private static JAXBElement dummyReportResponse() {
        return new JAXBElement<>(new QName("", ""), ProcessIndividualCaseSafetyReportResponseType.class, new ProcessIndividualCaseSafetyReportResponseType());
    }

    private ProcessIndividualCaseSafetyReportType testReport() {
        final ProcessIndividualCaseSafetyReportType theReport = new ProcessIndividualCaseSafetyReportType();
        final ICHICSRType e2bReport = new ICHICSRType();
        final ICHICSRMessageHeaderType e2bHeader = new ICHICSRMessageHeaderType();
        final IndividualCaseSafetyReportType safetyReportType = new IndividualCaseSafetyReportType();

        safetyReportType.setIchicsr(e2bReport);
        e2bReport.setIchicsrmessageheader(e2bHeader);
        e2bHeader.setMessagenumb("ONE_2_THREE_4_NUM");

        theReport.setIndividualCaseSafetyReport(safetyReportType);
        return theReport;
    }


}