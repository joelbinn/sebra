package se.inera.biverkningsrapportering;

import org.junit.Test;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.context.MessageContext;
import se.inera.biverkningsrapportering.validation.SchemaValidationException;
import se.inera.biverkningsrapportering.validation.SchemaValidator;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class PayloadValidationInterceptorTest {

    @Test
    public void handleRequest_calls_validator_and_fails() throws Exception {
        // Given:
        final Source emptyPayload = new StreamSource();
        final SchemaValidator validator = mockedSchemaValidator(emptyPayload);
        final MessageContext soapRequest = mockedMessageContext(emptyPayload);
        final PayloadValidationInterceptor interceptor = new PayloadValidationInterceptor(validator);

        // When:
        boolean bool = false;
        WebServiceClientException expectedExc = null;
        try {
            bool = interceptor.handleRequest(soapRequest);
        } catch (WebServiceClientException exc) {
            expectedExc = exc;
        }

        // Then:
        assertThat(expectedExc).isInstanceOf(SchemaValidationException.class).hasMessage("BAD_CLIENT!");
        verify(validator).validates(emptyPayload);
        assertThat(bool).isFalse();
    }


    private SchemaValidator mockedSchemaValidator(final Source somePayload) {
        SchemaValidator validator = mock(SchemaValidator.class);
        when(validator.validates(eq(somePayload))).thenThrow(new SchemaValidationException("BAD_CLIENT!"));
        return validator;
    }

    private MessageContext mockedMessageContext(final Source somePayload) {
        MessageContext msgCtx = mock(MessageContext.class);
        final WebServiceMessage theRequest = mock(WebServiceMessage.class);

        when(msgCtx.getRequest()).thenReturn(theRequest);
        when(theRequest.getPayloadSource()).thenReturn(somePayload);

        return msgCtx;
    }

}