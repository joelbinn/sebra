package se.inera.biverkningsrapportering.validation;

import org.junit.Test;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;

public class SchemaValidatorTest {

    @Test
    public void schema_validation_OK() throws Exception {
        // Given:
        File schemaFile = getFile("schemas/interactions/ProcessIndividualCaseSafetyReportInteraction/ProcessIndividualCaseSafetyReportResponder_1.0.xsd");
        Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(schemaFile);
        InputStream toValidate = getFileStream("testdata/test_report_anna.xml");

        // When:
        SchemaValidator schemaValidator = SchemaValidator.forSchema(schema);
        boolean validates = schemaValidator.validates(toValidate);

        // Then:
        assertThat(validates).isTrue();
    }

    @Test
    public void schema_validation_NOK_because_of_invalid_xml() throws Exception {
        // Given:
        File schemaFile = getFile("schemas/interactions/ProcessIndividualCaseSafetyReportInteraction/ProcessIndividualCaseSafetyReportResponder_1.0.xsd");
        Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(schemaFile);
        InputStream toValidate = getFileStream("testdata/SE-SEBRA-20140710153001-E2B_invalid.xml");

        // When:
        SchemaValidator schemaValidator = SchemaValidator.forSchema(schema);
        boolean validates = schemaValidator.validates(toValidate);

        // Then:
        assertThat(validates).isFalse();
    }

    private File getFile(final String relativePath) {
        return new File(getClass().getClassLoader().getResource(relativePath).getFile());
    }

    private InputStream getFileStream(String filePath) {
        return getClass().getClassLoader().getResourceAsStream(filePath);
    }

}