package se.inera.biverkningsrapportering.gateway;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.transport.http.HttpUrlConnection;

import javax.xml.namespace.QName;

import static org.mockito.Mockito.*;

public class SebraWSMessageCallbackTest {

    private static final QName EXP_LOGICAL_ADDRESS = new QName("urn:riv:itintegration:registry:1", "LogicalAddress");

    @Ignore
    @Test
    public void should_set_correlationId_as_http_request_header() throws Exception {
        // Given:
        final HttpUrlConnection wsConnection = mock(HttpUrlConnection.class);
        final SebraWSMessageCallback clsUnderTest = new SebraWSMessageCallback("DUM_MY_ADDR_13").withMessageNumb("ONE_2_THREE");
        final SebraWSMessageCallback sebraWSMessageCallback = spy(clsUnderTest);
        final SoapMessage soapMessage = mockedSoapMessage();

        // When:
        sebraWSMessageCallback.doWithMessage(soapMessage);

        // Then:
        verify(sebraWSMessageCallback.getWsConnection()).addRequestHeader("x-skltp-correlation-id", "ONE_2_THREE");
    }

    @Ignore
    @Test
    public void should_set_soap_action_as_http_request_header() throws Exception {
        // Given:
        final SebraWSMessageCallback clsUnderTest = new SebraWSMessageCallback("DUM_MY_ADDR_25");
        final SoapMessage soapMessage = mockedSoapMessage();

        // When:
        clsUnderTest.doWithMessage(soapMessage);

        // Then:
        verify(soapMessage).setSoapAction("urn:riv:lv:reporting:pharmacovigilance:ProcessIndividualCaseSafetyReportResponder:1:ProcessIndividualCaseSafetyReport");
    }
    @Ignore
    @Test
    public void shouldSetLogicalAddressAsSoapHeader() throws Exception {
        // Given:
        final String dum_my_addr_47 = "DUM_MY_ADDR_47";
        final SebraWSMessageCallback clsUnderTest = new SebraWSMessageCallback(dum_my_addr_47);
        final SoapMessage soapMessage = mock(SoapMessage.class);
        final SoapHeader soapHeader = mock(SoapHeader.class);
        final SoapHeaderElement headerElement = mock(SoapHeaderElement.class);

        when(soapMessage.getSoapHeader()).thenReturn(soapHeader);
        when(soapHeader.addHeaderElement(isA(QName.class))).thenReturn(headerElement);

        // When:
        clsUnderTest.doWithMessage(soapMessage);

        // Then:
        verify(soapHeader).addHeaderElement(EXP_LOGICAL_ADDRESS);
        verify(headerElement).setText(dum_my_addr_47);
    }

    //
    //
    private SoapMessage mockedSoapMessage() {
        final SoapMessage soapMessage = mock(SoapMessage.class);
        final SoapHeader dontCareH = mock(SoapHeader.class);
        final SoapHeaderElement dontCareEl = mock(SoapHeaderElement.class);

        when(soapMessage.getSoapHeader()).thenReturn(dontCareH);
        when(dontCareH.addHeaderElement(any())).thenReturn(dontCareEl);
        return soapMessage;
    }

}