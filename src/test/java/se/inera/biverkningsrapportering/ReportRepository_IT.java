package se.inera.biverkningsrapportering;

import com.mongodb.WriteResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import riv.lv.reporting.pharmacovigilance.v1.ParentType;
import smarttags.SmartTagType;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataMongoTest
@ActiveProfiles("autotest")
public class ReportRepository_IT {

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    private TestReportWrapperFactory testReportWrapperFactory;

    @Before
    @After
    public void cleanSlate() {
        reportRepository.drop();
    }

    @Test
    public void preservesSmarttagAsObjectAndString() throws Exception {
        //given
        ReportWrapper reportWrapper = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
        String reportId = reportWrapper.getReportId();
        reportRepository.save(reportWrapper);

        //when
        Optional<ReportWrapper> dbObject = reportRepository.findOneById(reportId);

        //then
        assertThat(dbObject).isPresent();
        ReportWrapper reportWrapperFromDb = dbObject.get();
        ParentType smarttagParent = reportWrapperFromDb.getReport().getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().getPatient().getParent();

        assertThat(smarttagParent).isInstanceOf(ParentTypeInternal.class);
        SmartTagType smartTagObject = ((ParentTypeInternal) smarttagParent).getSmarttag();
        assertThat(smartTagObject.getPatientfirstname()).isEqualTo("Anna");
        String smarttagString = smarttagParent.getParentmedicalrelevanttext();
        assertThat(smarttagString).isNotEmpty();
        assertThat(smarttagString).startsWith("&lt;");
        assertThat(smarttagString).endsWith("&gt;");
        assertThat(smarttagString).contains("Anna");
    }

    @Test
    public void writeNewReportAndFindById() throws Exception {
        //given
        ReportWrapper reportWrapper = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
        String reportId = reportWrapper.getReportId();
        reportRepository.save(reportWrapper);

        //when
        Optional<ReportWrapper> dbObject = reportRepository.findOneById(reportId);

        //then
        assertThat(dbObject).isPresent();
        ReportWrapper reportWrapperFromDb = dbObject.get();
        assertThat(reportWrapperFromDb.getReportId()).isEqualTo(reportId);
        assertThat(reportWrapperFromDb.getReport().getIndividualCaseSafetyReport().getIchicsr().getIchicsrmessageheader().getMessagenumb())
                .contains(reportId);
        assertThat(reportWrapperFromDb.getStatus()).isEqualTo(Status.DRAFT);
    }

    @Test
    public void writtenReportsAreIncludedInFindAllResult() throws Exception {
        //given
        String reportId1 = "id1";
        String reportId2 = "id2";
        String reportId3 = "id3";
        reportRepository.save(getEmptyReportWrapper(reportId1, Status.DRAFT));
        reportRepository.save(getEmptyReportWrapper(reportId2, Status.DRAFT));
        reportRepository.save(getEmptyReportWrapper(reportId3, Status.SUBMITTED));

        //when
        List<ReportWrapper> reportWrappers = reportRepository.findAll();

        //then
        assertThat(reportWrappers).isNotNull();
        assertThat(reportWrappers).isNotEmpty();
        assertThat(reportWrappers.size()).isEqualTo(3);
        assertThat(reportWrappers).extracting(ReportWrapper::getReportId).contains(reportId1, reportId2, reportId3);
    }

    @Test
    public void searchNONExistingReportInEmptyDb() throws Exception {
        //given
        //empty db

        //when
        Optional<ReportWrapper> oneById = reportRepository.findOneById("someNonexistingReportId");

        // Then:
        assertThat(oneById).isNotPresent();
    }

    @Test
    public void searchNONExistingReportInNonEmptyDb() throws Exception {
        //given
        String reportId1 = "id1";
        String reportId2 = "id2";
        reportRepository.save(getEmptyReportWrapper(reportId1, Status.DRAFT));
        reportRepository.save(getEmptyReportWrapper(reportId2, Status.DRAFT));

        //when
        Optional<ReportWrapper> oneById = reportRepository.findOneById("someNonexistingReportId");

        // Then:
        assertThat(oneById).isNotPresent();
    }

    @Test
    public void updateExistingReport() throws Exception {
        //given
        // - save a report in db
        ReportWrapper reportWrapper = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
        String reportId = reportWrapper.getReportId();
        String origOccurCountry = reportWrapper.getReport().getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().getOccurcountry();
        reportRepository.save(reportWrapper);
        // - update the report
        String newOccurCountry = "US";
        reportWrapper.getReport().getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().setOccurcountry("US");

        // When:
        // - save the updated report
        WriteResult writeResult = reportRepository.save(reportWrapper);
        // - read the report (to validate the updated field)
        Optional<ReportWrapper> oneById = reportRepository.findOneById(reportId);

        // Then:
        assertThat(writeResult.wasAcknowledged()).isTrue();
        assertThat(writeResult.getUpsertedId()).isEqualTo(reportId);
        assertThat(oneById.isPresent());
        assertThat(oneById.get().getReport().getIndividualCaseSafetyReport().getIchicsr().getSafetyreport().getOccurcountry()).isEqualTo(newOccurCountry);
        assertThat(newOccurCountry).isNotEqualTo(origOccurCountry);//guard assert to make sure the test is valid (if test xml is changed)
    }

    @Test
    public void canGetDraftReportIdByStatus() throws Exception {
        //given
        ReportWrapper reportWrapper = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
        String reportId = reportWrapper.getReportId();
        reportRepository.save(reportWrapper);

        //when
        List<String> reportIdsByStatus = reportRepository.getReportIdsByStatus(Status.DRAFT);

        //then
        assertThat(reportIdsByStatus).contains(reportId);
    }

    @Test
    public void submittedReportIsNotIncludedInGetByStatusDraft() throws Exception {
        //given
        String reportId = "someReportId";
        reportRepository.save(getEmptyReportWrapper(reportId, Status.SUBMITTED));

        //when
        List<String> reportIdsByStatus = reportRepository.getReportIdsByStatus(Status.DRAFT);

        //then
        assertThat(reportIdsByStatus).doesNotContain(reportId);
    }

    @Test
    public void canGetSeveralDraftReportIdsByStatus() throws Exception {
        //given
        String reportId1 = "id1";
        String reportId2 = "id2";
        String reportId3 = "id3";
        reportRepository.save(getEmptyReportWrapper(reportId1, Status.DRAFT));
        reportRepository.save(getEmptyReportWrapper(reportId2, Status.DRAFT));
        reportRepository.save(getEmptyReportWrapper(reportId3, Status.SUBMITTED));

        //when
        List<String> reportIdsByStatus = reportRepository.getReportIdsByStatus(Status.DRAFT);

        //then
        assertThat(reportIdsByStatus).contains(reportId1, reportId2);
        assertThat(reportIdsByStatus).doesNotContain(reportId3);
    }

    private ReportWrapper getEmptyReportWrapper(String reportId, Status status) {
        return new ReportWrapper(reportId, status, null);
    }


}
