package se.inera.biverkningsrapportering;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

//NOTE: using empty config to avoid initializing full application
@EnableAutoConfiguration
public class EmptyTestConfig {
}
