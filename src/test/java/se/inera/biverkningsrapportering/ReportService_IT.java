package se.inera.biverkningsrapportering;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.WriteResult;
import io.restassured.RestAssured;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ProcessIndividualCaseSafetyReportResponseType;
import riv.lv.reporting.pharmacovigilance.processindividualcasesafetyreportresponder.v1.ResultCodeEnum;

import java.io.InputStream;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("autotest")
public class ReportService_IT {

    @LocalServerPort
    int localServerPort;

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    TestReportWrapperFactory testReportWrapperFactory;

    private static final String RESOURCE_PATH = "/report";
    private String reportIdExistingReport;

    @MockBean
    ProcessReportGateway processReportGateway;

    @Before
    public void setUP() throws Exception {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = localServerPort;

        ProcessIndividualCaseSafetyReportResponseType stubbedResponse = getStubbedResponse(ResultCodeEnum.OK, "Hardcoded result text from mock");

        Mockito.when(processReportGateway.send(Mockito.any())).thenReturn(stubbedResponse);

        ReportWrapper wrappedTestReport = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
        WriteResult writeResult = reportRepository.save(wrappedTestReport);
        reportIdExistingReport = writeResult.getUpsertedId().toString();
    }

    @Test
    public void should_find_report_by_id() {
        String expectedSafetyReportId = "SE-SEBRA-" + reportIdExistingReport;
        given()
                .when()
                .get(RESOURCE_PATH + "/" + reportIdExistingReport)
                .then()
                .log().ifValidationFails()
                .assertThat()
                .statusCode(200)
                .contentType("application/json")
                .body("_id", equalTo(reportIdExistingReport))
                .body("report.individualCaseSafetyReport.ichicsr.safetyreport.safetyreportid", equalTo(expectedSafetyReportId));
        //TODO: validate more fields?
    }

    @Test
    public void expecting_a_list_of_reports_in_draft_mode() {
        given()
                .when()
                .get(RESOURCE_PATH + "?status=DRAFT")
                .then()
                .log().ifValidationFails()
                .assertThat()
                .statusCode(200)
                .contentType("application/json")
                .body(
                        "reportIds", hasSize(greaterThan(1)),
                        "reportIds", hasItem(reportIdExistingReport)
                );
    }

    @Test
    public void should_return_status_400_on_INvalid_status() {
        given()
                .when()
                .get(RESOURCE_PATH + "?status=some-illegal-status")
                .then()
                .log().ifValidationFails()
                .assertThat()
                .statusCode(400);
    }

    @Test
    public void should_return_404_on_unknown_report_id() {
        given()
                .when()
                .get(RESOURCE_PATH + "/" + "non-existing-report-id")
                .then()
                .log().ifValidationFails()
                .assertThat()
                .statusCode(404);
    }

    @Test
    public void bad_request_when_no_reportId_is_given() {
        given()
                .when()
                .get(RESOURCE_PATH)
                .then()
                .log().ifValidationFails()
                .assertThat()
                .statusCode(400)
                .contentType("text/plain")
                .body(containsString("Use '/report/{reportId}' or /report?status={status}"));

    }

    @Test
    public void on_report_update_return_status_204() throws Exception {
        given().header("Content-type", "application/json").body(sampleReportAsJSON("testdata/sample_report_1.json"))
                .when()
                .put(RESOURCE_PATH + "/" + reportIdExistingReport)
                .then().log().ifValidationFails()
                .assertThat()
                .statusCode(204) // HTTP NO_CONTENT
                .contentType("");
    }

    @Test
    public void on_report_submit_return_status_205() throws Exception {
        given().header("Content-type", "application/json").body(sampleReportAsJSON("testdata/sample_report_1.json"))
                .when()
                .put(RESOURCE_PATH + "/" + reportIdExistingReport + "?submit=true")
                .then().log().ifValidationFails()
                .assertThat()
                .statusCode(205) // HTTP RESET_CONTENT
                .contentType("");
    }

    @Test
    public void on_successful_submit_of_draft_report_update_status() throws Exception {
        //given
        // a report in DRAFT status
        ReportWrapper wrappedNewReport = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
        WriteResult writeResult = reportRepository.save(wrappedNewReport);
        String reportId = writeResult.getUpsertedId().toString();
        assertThat(wrappedNewReport.getStatus()).isEqualTo(Status.DRAFT);//guard assert

        //when
        // submitted
        given().header("Content-type", "application/json").body(asJSON(wrappedNewReport))
                .when()
                .put(RESOURCE_PATH + "/" + reportId + "?submit=true");

        //then
        // report is in SUBMITTED status
        Optional<ReportWrapper> wrappedUpdatedReport = reportRepository.findOneById(reportId);
        assertThat(wrappedUpdatedReport.get().getStatus()).isEqualTo(Status.SUBMITTED);
    }

    @Test
    //Note: as long as the report is sent (i.e. passes sebra internal validations),
    //status must be updated and report NOT allowed to be re-submitted
    // (since correlation id is set to messagenumb and must be unique per request)
    // i.e. if re-submission is to be allowed, handling of corr id has to be adjusted
    public void on_UNsuccessful_submit_of_draft_report_update_status_anyway() throws Exception {
        //given
        // a report in DRAFT status
        ReportWrapper wrappedNewReport = testReportWrapperFactory.getReportWrapperWithUniqueIdUsingPayload("testdata/test_report_anna.xml");
        WriteResult writeResult = reportRepository.save(wrappedNewReport);
        String reportId = writeResult.getUpsertedId().toString();
        assertThat(wrappedNewReport.getStatus()).isEqualTo(Status.DRAFT);//guard assert
        // and a broken web service
        ProcessIndividualCaseSafetyReportResponseType stubbedResponse = getStubbedResponse(ResultCodeEnum.ERROR, "Schema validation error...");
        Mockito.when(processReportGateway.send(Mockito.any())).thenReturn(stubbedResponse);

        //when
        // submitted
        given().header("Content-type", "application/json").body(asJSON(wrappedNewReport))
                .when()
                .put(RESOURCE_PATH + "/" + reportId + "?submit=true");

        //then
        // report is in SUBMITTED status
        Optional<ReportWrapper> wrappedUpdatedReport = reportRepository.findOneById(reportId);
        assertThat(wrappedUpdatedReport.get().getStatus()).isEqualTo(Status.SUBMITTED);
    }

    @Test
    public void on_report_submit_with_queryParam_return_status_205() throws Exception {
        given().header("Content-type", "application/json").body(sampleReportAsJSON("testdata/sample_report_1.json"))
                .queryParam("submit", true)
                .when()
                .put(RESOURCE_PATH + "/" + reportIdExistingReport)
                .then().log().ifValidationFails()
                .assertThat()
                .statusCode(205) // HTTP RESET_CONTENT
                .contentType("");
    }

    @Test
    public void PUTting_report_with_no_ID_yields_status_400() throws Exception {
        // The invalid_report_whith_no_id.json is missing top-level '_id' field (which is what we need for this test)
        given().header("Content-type", "application/json").body(sampleReportAsJSON("testdata/invalid_report_whith_no_id.json"))
                .when()
                .put(RESOURCE_PATH + "/" + reportIdExistingReport)
                .then().log().ifValidationFails()
                .assertThat()
                .statusCode(400) // HTTP BAD_REQUEST
                .contentType("text/plain")
                .body(containsString("invalid report"))
        ;
    }

    @Test
    public void missing_report_yields_status_400() throws Exception {
        given().header("Content-type", "application/json").body(sampleReportAsJSON("testdata/invalid_no_report.json"))
                .when()
                .put(RESOURCE_PATH + "/" + reportIdExistingReport)
                .then().log().ifValidationFails()
                .assertThat()
                .statusCode(400) // HTTP BAD_REQUEST
                .contentType("text/plain")
                .body(containsString("invalid report"))
        ;
    }

    private String sampleReportAsJSON(final String sampleFile) throws Exception {
        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(sampleFile);
        return IOUtils.toString(resourceAsStream);
    }

    private String asJSON(ReportWrapper reportWrapper) throws JsonProcessingException {
        ObjectMapper toJsonMapper = new ObjectMapper();
        return toJsonMapper.writeValueAsString(reportWrapper);
    }

    private ProcessIndividualCaseSafetyReportResponseType getStubbedResponse(final ResultCodeEnum resultCode, final String someText) {
        ProcessIndividualCaseSafetyReportResponseType stubbedResponse = new ProcessIndividualCaseSafetyReportResponseType();
        stubbedResponse.setResultCode(resultCode);
        stubbedResponse.setResultText(someText);
        return stubbedResponse;
    }
}