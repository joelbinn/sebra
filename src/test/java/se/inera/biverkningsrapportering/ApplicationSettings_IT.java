package se.inera.biverkningsrapportering;

import org.junit.Test;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationSettings_IT {

    ApplicationSettings applicationSettings = new ApplicationSettings();

    @Test
    public void should_validate_because_SSL_is_not_required() throws Exception {
        // Given:
        applicationSettings.setSslEnabled(false); // SSL is disabled by default

        // When:
        applicationSettings.validate();

        // Then:
        // No exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void thows_an_exception_because_of_missing_settings() throws Exception {
        // Given:
        applicationSettings.setSslEnabled(true);

        // When:
        String errMsg = null;
        try {
            applicationSettings.validate();
        } catch (Exception e) {
            errMsg = e.getMessage();
            throw e;
        } finally {
            // Then:
            assertThat(errMsg).contains("required but not set");
        }

    }

    @Test
    public void verifying_default_profile() throws Exception {
        // Given:
        YamlPropertySourceLoader loader = new YamlPropertySourceLoader();

        // When:
        PropertySource<?> appYaml = loader.load("APP_SETTINGS", new ClassPathResource("application.yml"), "default");

        // Then:
        assertThat(appYaml.getProperty("spring.profiles")).isEqualTo("default");
        assertThat(appYaml.getProperty("spring.data.mongodb.host")).isEqualTo("localhost");
        assertThat(appYaml.getProperty("spring.data.mongodb.port")).isEqualTo(27017);
        assertThat(appYaml.getProperty("spring.data.mongodb.database")).isEqualTo("sebra");
        assertThat(appYaml.getProperty("server.port")).isEqualTo(8080);
        assertThat(appYaml.getProperty("server.ssl.enabled")).isNull();
        assertThat(appYaml.getProperty("sebra.processIndividualCaseSafetyReportTypeUrl")).isEqualTo("https://tjp.nordicmedtest.se/sebra/mockProcessIndividualCaseSafetyReport");
        assertThat(appYaml.getProperty("sebra.processIndividualCaseSafetyReportLogicalAddress")).isEqualTo(2021004078);
        assertThat(appYaml.getProperty("sebra.keystore")).isEqualTo("sebra.keystore.jks");
        assertThat(appYaml.getProperty("sebra.keystorePassword")).isEqualTo("<keystore password>");
        assertThat(appYaml.getProperty("sebra.truststore")).isEqualTo("sebra.truststore.jks");
        assertThat(appYaml.getProperty("sebra.truststorePassword")).isEqualTo("<truststore password>");

    }

    @Test
    public void verifying_integration_profile() throws Exception {
        // Given:
        YamlPropertySourceLoader loader = new YamlPropertySourceLoader();

        // When:
        PropertySource<?> appYaml = loader.load("APP_SETTINGS_DEV", new ClassPathResource("application-autotest.yml"), "autotest");

        // Then:
        assertThat(appYaml.getProperty("spring.data.mongodb.host")).isNull();
        assertThat(appYaml.getProperty("spring.data.mongodb.port")).isEqualTo(0);
        assertThat(appYaml.getProperty("spring.data.mongodb.database")).isNull();
        assertThat(appYaml.getProperty("server.port")).isNull();
        assertThat(appYaml.getProperty("server.ssl.enabled")).isNull();
        assertThat(appYaml.getProperty("sebra.processIndividualCaseSafetyReportTypeUrl")).isEqualTo("http://localhost:8088/mockProcessIndividualCaseSafetyReport");
        assertThat(appYaml.getProperty("sebra.processIndividualCaseSafetyReportLogicalAddress")).isEqualTo("dummyLogicalAddressForAutotest");
        assertThat(appYaml.getProperty("sebra.keystore")).isNull();
        assertThat(appYaml.getProperty("sebra.keystorePassword")).isNull();
        assertThat(appYaml.getProperty("sebra.truststore")).isNull();
        assertThat(appYaml.getProperty("sebra.truststorePassword")).isNull();

    }

}